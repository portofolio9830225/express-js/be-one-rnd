const _ = require("lodash");
const jwt_decode = require("jwt-decode");

function isEmptyObject(obj) {
  return obj == null || !Object.keys(obj).length;
}

function isEmptyArray(arr) {
  return arr == null || !arr.length;
}

function hasKey(obj, key) {
  return _.has(obj, key) && obj[key] != null;
}

function distinct(obj) {
  return obj.filter((value, index, self) => {
    return self.indexOf(value) === index;
  });
}

function getCurrrentUser(req) {
  const userToken = req.header('x-user-token')
  const decodeToken = jwt_decode(userToken);
  return decodeToken.details.hris_org_tree.current_person
}

module.exports = {
  getCurrrentUser,
  isEmptyObject,
  isEmptyArray,
  hasKey,
  distinct,
};
