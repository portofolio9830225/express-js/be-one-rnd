FROM node:16-alpine 
# Specify where our app will live in the container
WORKDIR /app
RUN apk --update add rsync
RUN apk add tzdata
ENV TZ="Asia/Jakarta"

# add the node_modules folder to $PATH
ENV PATH /app/node_modules/.bin:$PATH
 
COPY package*.json /app/
COPY ./yarn.lock /app/
RUN npm install
COPY . /app
EXPOSE 3050
# start express server 

CMD [ "npm", "start" ] 