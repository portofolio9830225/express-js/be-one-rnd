const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:segment-module");
const mongoose = require("mongoose");
const helper = require("../common/helper");
const escapeRegex = require("../common/escapeRegex");
const db = require("../models");
const zeroPad = require("../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const PO_Detail = db.po_detail;

async function readByVendorAndMaterial(query) {
  try {
    let condition = {
      is_active: true,
      po_vendor: query.vendor_code,
      material_code: query.material_code,
    };

    const ck = await PO_Detail.findOne(condition)
      .sort({ po_date: -1 })
      .select("-__v")
      .lean();
    if (helper.isEmptyObject(ck)) {
      return {};
    }
    return ck;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  readByVendorAndMaterial,
};
