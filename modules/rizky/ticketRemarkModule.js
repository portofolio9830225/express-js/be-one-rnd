const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-checklist:ticket-remark-module");
const helper = require("../../common/helper");
const db = require("../../models");
const TicketRemarkModel = db.ticket_remark;

async function readAll(query) {
    //query string
    let condition = {
        is_active: true,
    };

    const customerAll = await TicketRemarkModel.find(condition).lean();
    if (helper.isEmptyObject(customerAll)) {
        return {
            foundData: [],
            currentPage: page,
            countPages: 0,
            countData: 0,
        };
    }
    let result = [];

    customerAll.forEach(async (_ticket_remark, index) => {
        let _ticket_remark_result = {
            value: _ticket_remark.name,
            text: _ticket_remark.name,
            label:  _ticket_remark.name
        };
        result.push(_ticket_remark_result);
    });

    return {
        countData: Object.keys(customerAll).length,
        foundData: result,
    };
}

module.exports = {
    readAll
}