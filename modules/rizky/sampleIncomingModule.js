const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:registrasi-ticket-module");
const helper = require("../../common/helper");
const db = require("../../models");
const moment = require("moment");
const SampleIncoming = db.sample_incoming;
const File = db.file;
const GeneralSettings = db.general_settings;
const { ObjectId } = require("mongodb");
const FormData = require("form-data");
const path = require("path");
const { readFileSync, unlinkSync } = require("fs");
const axios = require("axios");
const fs = require("fs");
const URL_FILE_SERVICE = "/api/file/onedrive/read-file/";
const jwt = require("jsonwebtoken");
const dateFormat = require("dateformat");
const MailModule = require("../mailModule");

const createSchema = Joi.object({
  material_name: Joi.string().required(),
  list_document: Joi.string().allow(null).allow(""),
  clasification: Joi.string().required(),
  manufacture: Joi.string().allow(null).allow(""),
  supplier: Joi.string().allow(null).allow(""),
  origin: Joi.string().allow(null).allow(""),
  pic: Joi.string().allow(null).allow(""),
  pic_name: Joi.string().allow(null).allow(""),
  pic_email: Joi.string().allow(null).allow(""),
  files: Joi.required(),
  date_receive: Joi.date().required(),
  remark: Joi.string().allow(null).allow(""),
  Note: Joi.string().allow(null).allow(""),
  qty: Joi.string().required(),
  uom: Joi.string().required(),
  location: Joi.string().required(),
  app_token: Joi.string().required(),
});

const copySchema = Joi.object({
  material_name: Joi.string().required(),
  list_document: Joi.string().allow(null).allow(""),
  clasification: Joi.string().required(),
  manufacture: Joi.string().allow(null).allow(""),
  supplier: Joi.string().allow(null).allow(""),
  origin: Joi.string().allow(null).allow(""),
  pic: Joi.string().allow(null).allow(""),
  pic_name: Joi.string().allow(null).allow(""),
  pic_email: Joi.string().allow(null).allow(""),
  files: Joi.required(),
  date_receive: Joi.date().required(),
  remark: Joi.string().allow(null).allow(""),
  Note: Joi.string().allow(null).allow(""),
  qty: Joi.string().required(),
  uom: Joi.string().required(),
  location: Joi.string().required(),
  app_token: Joi.string().required(),
  documnet: Joi.string().allow(null)
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string().required(),
  details: Joi.array().items(
    Joi.object({
      name: Joi.string().required(),
    })
  ),
});

const idSchema = Joi.object({
  _id: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

function validateCopySchema(schema) {
  return copySchema.validate(schema);
}

function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}

function validateIdSchema(schema) {
  return idSchema.validate(schema);
}

async function copySampleIncoming(payload_params, currentUser) {
  let payload = payload_params;
  debug(payload);
  const validate = validateCopySchema(payload);
  if (validate.error) {
    return error.errorReturn({ message: validate.error.details[0].message });
  }

  debug(payload.files, " payload.files");

  if (payload.files.length > 0) {
    if (payload.files[0].size / (1024 * 1024) > 2) {
      return error.errorReturn({ message: "Max file size 2MB" });
    }
    let parameters = {
      method: "POST",
      url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
      headers: {
        "x-application-token": `${payload.app_token}`,
        "x-public-key": `${process.env.PUBLIC_KEY}`,
      },
    };

    const form = new FormData();
    form.append("apps", process.env.NAME);

    //upload files to onedrive, get the link
    payload.files.forEach((f) => {
      const readDir = path.join(__dirname, `../../${f.path}`);
      const readFile = readFileSync(readDir);
      form.append("upload_file", readFile, f.path);
    });
    parameters.data = form;
    parameters.maxContentLength = Infinity;
    parameters.maxBodyLength = Infinity;
    parameters.headers["Content-Type"] =
      "multipart/form-data;boundary=" + form.getBoundary();

    const result = await axios(parameters)
      .then(function async(response) {
        return response.data;
      })
      .catch(function (error) {
        debug(error, "=> error callApiByArea");
        return [];
      });

    OneDriveId = result[0].onedrive_id;

    for (let f of payload.files) {
      // remove files when finish make call
      unlinkSync(path.join(__dirname, `../../${f.path}`));
    }

    if (result[0]) {
      const OneDriveId = result[0].onedrive_id;
      payload.documnet = OneDriveId;
      let filePayload = {
        onedrive_id: OneDriveId,
        originalname: result[0].originalname,
        mimetype: result[0].mimetype,
        apps: result[0].apps,
        created_at: new Date().toISOString(),
        is_active: true
      };
      await new File(filePayload).save();
    }
  }

  payload.is_active = true;
  payload.created_at = new Date();
  payload.updated_at = new Date();
  payload.created_by = currentUser.username;

  const sampleIncoming = await new SampleIncoming(payload).save();
  return {
    data: sampleIncoming,
    status: "success sample incoming has copied",
  };
}

async function createSampleIncoming(payload_params, currentUser) {
  let payload = payload_params;
  debug(payload);
  const validate = validateCreateSchema(payload);
  if (validate.error) {
    return error.errorReturn({ message: validate.error.details[0].message });
  }

  let OneDriveId = null;
  debug(payload.files, " payload.files");

  if (payload.files.length > 0) {
    if (payload.files[0].size / (1024 * 1024) > 2) {
      return error.errorReturn({ message: "Max file size 2MB" });
    }
    let parameters = {
      method: "POST",
      url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
      headers: {
        "x-application-token": `${payload.app_token}`,
        "x-public-key": `${process.env.PUBLIC_KEY}`,
      },
    };

    const form = new FormData();
    form.append("apps", process.env.NAME);

    //upload files to onedrive, get the link
    payload.files.forEach((f) => {
      const readDir = path.join(__dirname, `../../${f.path}`);
      const readFile = readFileSync(readDir);
      form.append("upload_file", readFile, f.path);
    });
    parameters.data = form;
    parameters.maxContentLength = Infinity;
    parameters.maxBodyLength = Infinity;
    parameters.headers["Content-Type"] =
      "multipart/form-data;boundary=" + form.getBoundary();

    const result = await axios(parameters)
      .then(function async(response) {
        return response.data;
      })
      .catch(function (error) {
        debug(error, "=> error callApiByArea");
        return [];
      });

    OneDriveId = result[0].onedrive_id;

    for (let f of payload.files) {
      // remove files when finish make call
      unlinkSync(path.join(__dirname, `../../${f.path}`));
    }

    if (result[0]) {
      const OneDriveId = result[0].onedrive_id;
      payload.documnet = OneDriveId;
      let filePayload = {
        onedrive_id: OneDriveId,
        originalname: result[0].originalname,
        mimetype: result[0].mimetype,
        apps: result[0].apps,
        created_at: new Date().toISOString(),
        is_active: true,
      };
      await new File(filePayload).save();
    }
  }

  payload.is_active = true;
  payload.created_at = new Date();
  payload.updated_at = new Date();
  payload.created_by = currentUser.username;
  payload.send_notif = [];

  const sampleIncoming = await new SampleIncoming(payload).save();
  const email = await GeneralSettings.find({
    key: "mail_head_rnd",
    is_active: true,
  });
  if (email) {
    const body = `<p>Dear Head RND, </p><p>This is information of incoming sample :</p><br><table role="presentation" border="1" cellpadding="2" cellspacing="0" id="tabel_set"> <tbody> <tr> <td width="13">Date</td><td>${sampleIncoming.date_receive}</td></tr><tr> <td>Material</td><td>${sampleIncoming.material_name}</td></tr><tr> <td>Clasification</td><td>${sampleIncoming.clasification}</td></tr><tr> <td>Qty </td><td>${sampleIncoming.qty}</td></tr><tr> <td>Uom</td><td>${sampleIncoming.uom}</td></tr><tr> <td>PIC</td><td>${sampleIncoming.pic}</td></tr><tr> <td>manufacture</td><td>${sampleIncoming.manufacture}</td></tr><tr> <td>supplier</td><td>${sampleIncoming.supplier}</td></tr><tr> <td>origin</td><td>${sampleIncoming.origin}</td></tr><tr> <td>Remarkt </td><td><span style="color: red;">${sampleIncoming.remark}</span> </td></tr><tr> <td>Note</td><td>${sampleIncoming.Note}</td></tr></tbody> </table>`;
    const send = {
      subject: "One RND - Sample Incoming",
      body: body,
      send_to: [{ name: "-", address: `${email[0].value}` }],
      send_cc: [],
    };
    await MailModule.sendEmail(
      send.subject,
      send.body,
      send.send_to,
      send.send_cc
    );
  }

  const body = `<p>Dear ${sampleIncoming.pic_name}, </p><p>This is information of incoming sample :</p><br><table role="presentation" border="1" cellpadding="2" cellspacing="0" id="tabel_set"> <tbody> <tr> <td width="13">Date</td><td>${sampleIncoming.date_receive}</td></tr><tr> <td>Material</td><td>${sampleIncoming.material_name}</td></tr><tr> <td>Clasification</td><td>${sampleIncoming.clasification}</td></tr><tr> <td>Qty </td><td>${sampleIncoming.qty}</td></tr><tr> <td>Uom</td><td>${sampleIncoming.uom}</td></tr><tr> <td>PIC</td><td>${sampleIncoming.pic}</td></tr><tr> <td>manufacture</td><td>${sampleIncoming.manufacture}</td></tr><tr> <td>supplier</td><td>${sampleIncoming.supplier}</td></tr><tr> <td>origin</td><td>${sampleIncoming.origin}</td></tr><tr> <td>Remarkt </td><td><span style="color: red;">${sampleIncoming.remark}</span> </td></tr><tr> <td>Note</td><td>${sampleIncoming.Note}</td></tr></tbody> </table>`;
  const send = {
    subject: "One RND - Sample Incoming",
    body: body,
    send_to: [{ name: "-", address: `${payload_params.pic_email}` }],
    send_cc: [],
  };

  const resultMail = await MailModule.sendEmail(
    send.subject,
    send.body,
    send.send_to,
    send.send_cc
  );

  return {
    data: sampleIncoming,
    status: "success sample incoming has created",
  };
}

async function readAllSampleIncoming(query) {
  //query string

  let condition = {
    is_active: true,
  };

  let filter = { is_active: true };

  if (query?.keyword) {
    switch (query.type) {
      case "PIC":
        filter = {
          ...filter,
          pic_name: { $regex: ".*" + query.keyword + ".*", $options: "i" },
        };
        break;
      case "Manufacture":
        filter = {
          ...filter,
          manufacture: { $regex: ".*" + query.keyword + ".*", $options: "i" },
        };
        break;
      case "Supplier":
        filter = {
          ...filter,
          supplier: { $regex: ".*" + query.keyword + ".*", $options: "i" },
        };
        break;
      default:
        if (query.keyword !== "undefined") {
          filter = {
            ...filter,
            clasification: {
              $regex: ".*" + query.keyword + ".*",
              $options: "i",
            },
          };
        }
    }
  }

  if (query?.date_start && query?.date_end) {
    filter = {
      ...filter,
      created_at: {
        $gte: moment(query.date_start).format("YYYY-MM-DD"),
        $lte: moment(query.date_end).format("YYYY-MM-DD"),
      },
    };
  }

  // sort
  const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
  const sortField = query?.sortField ?? "created_at";
  const sort = { [sortField]: sortOrder };

  // skip
  const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
  const page = query?.page ? Number(query.page) : 1;
  const skip = sizePerPage * page - sizePerPage;

  // limit
  const limit = sizePerPage;

  // find data
  let found_data = [];

  data_xls = await SampleIncoming.find(filter)
    .sort(sort)
    .collation({ locale: "en_US", numericOrdering: true })
    .select("-__v")
    .lean();
  found_data = await SampleIncoming.find(filter)
    .sort(sort)
    .collation({ locale: "en_US", numericOrdering: true })
    .select("-__v")
    .skip(skip)
    .limit(limit)
    .lean();
  // if empty data
  if (helper.isEmptyArray(found_data)) {
    const result = {
      foundData: [],
      dataXls: [],
      currentPage: page,
      countPages: 0,
      countData: 0,
    };
    debug(result);
    debug("===> RESULT READ SAMPLE INCOMING");
    return result;
  }

  //
  const count_data = await SampleIncoming.countDocuments(filter);
  const current_page = query.showAll ? 1 : page;
  const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
  const result = {
    foundData: found_data,
    dataXls: data_xls,
    currentPage: current_page,
    countPages: count_page,
    countData: count_data,
  };

  return result;
}

async function updateSampleIncoming(payload) {
  const filter = { _id: ObjectId(payload._id) };
  let update = payload;
  update.date = new Date(payload.date);

  let parameters = {
    method: "POST",
    url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
    headers: {
      "x-application-token": `${payload.app_token}`,
      "x-public-key": `${process.env.PUBLIC_KEY}`,
    },
  };

  const form = new FormData();
  form.append("apps", process.env.NAME);

  //upload files to onedrive, get the link
  if (payload.files) {
    payload.files.forEach((f) => {
      const readDir = path.join(__dirname, `../../${f.path}`);
      const readFile = readFileSync(readDir);
      form.append("upload_file", readFile, f.path);
    });
  }
  parameters.data = form;
  parameters.maxContentLength = Infinity;
  parameters.maxBodyLength = Infinity;
  parameters.headers["Content-Type"] =
    "multipart/form-data;boundary=" + form.getBoundary();

  const result = await axios(parameters)
    .then(function async(response) {
      return response.data;
    })
    .catch(function (error) {
      debug(error, "=> error callApiByArea");
      return [];
    });

  if (payload.files) {
    for (let f of payload.files) {
      // remove files when finish make call
      unlinkSync(path.join(__dirname, `../../${f.path}`));
    }
  }

  if (result[0]) {
    const OneDriveId = result[0].onedrive_id;
    update.documnet = OneDriveId;
    let filePayload = {
      onedrive_id: OneDriveId,
      originalname: result[0].originalname,
      mimetype: result[0].mimetype,
      apps: result[0].apps,
      created_at: new Date().toISOString(),
      is_active: true,
    };
    await new File(filePayload).save();
  }

  if (payload.send_notif) update.send_notif = payload.send_notif;

  let sampleIncoming = await SampleIncoming.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
    new: true,
  });

  if (payload.send_notif) {
    payload.send_notif.forEach(async (x) => {
      const body = `<p>Dear sdr/i. ${x.name}, </p><p>This is information of incoming sample :</p><br><table role="presentation" border="1" cellpadding="2" cellspacing="0" id="tabel_set"> <tbody> <tr> <td width="13">Date</td><td>${sampleIncoming.date_receive}</td></tr><tr> <td>Material</td><td>${sampleIncoming.material_name}</td></tr><tr> <td>Clasification</td><td>${sampleIncoming.clasification}</td></tr><tr> <td>Qty </td><td>${sampleIncoming.qty}</td></tr><tr> <td>Uom</td><td>${sampleIncoming.uom}</td></tr><tr> <td>PIC</td><td>${sampleIncoming.pic}</td></tr><tr> <td>manufacture</td><td>${sampleIncoming.manufacture}</td></tr><tr> <td>supplier</td><td>${sampleIncoming.supplier}</td></tr><tr> <td>origin</td><td>${sampleIncoming.origin}</td></tr><tr> <td>Remarkt </td><td><span style="color: red;">${sampleIncoming.remark}</span> </td></tr><tr> <td>Note</td><td>${sampleIncoming.Note}</td></tr></tbody> </table>`;
      const send = {
        subject: "One RND - Sample Incoming",
        body: body,
        send_to: [{ name: "-", address: `${x.mail}` }],
        send_cc: [],
      };
      await MailModule.sendEmail(
        send.subject,
        send.body,
        send.send_to,
        send.send_cc
      );
    });
  }

  if (helper.isEmptyObject(sampleIncoming)) {
    return error.errorReturn({ message: "Sample Incoming not found" });
  }

  return {
    data: sampleIncoming,
    status: "Data has been updated successfully",
  };
}

async function deleteSampleIncoming(payload) {
  const validate = validateIdSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  const filter = { _id: payload._id };
  const update = {
    updated_at: new Date(),
    is_active: false,
  };
  let sampleIncomingDelete = await SampleIncoming.findOneAndUpdate(
    filter,
    update,
    {
      useFindAndModify: false,
      new: true,
    }
  );
  if (helper.isEmptyObject(sampleIncomingDelete)) {
    return error.errorReturn({ message: "Sample Incoming not found" });
  }

  return {
    data: sampleIncomingDelete,
    status: "Data has been deleted successfully",
  };
}

async function downloadFile(payload) {
  let id = "";
  Object.entries(payload.id).forEach(([key, val]) => {
    id = key;
  });
  let parameters = {
    method: "GET",
    url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}${URL_FILE_SERVICE}?onedrive_id=${id}`,
    headers: {
      "x-application-token": `${payload.app_token}`,
      "x-public-key": `${process.env.PUBLIC_KEY}`,
    },
  };
  const res = await axios(parameters)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      debug(e);
    });
  debug(res);
  return { res };
}

module.exports = {
  createSampleIncoming,
  copySampleIncoming,
  updateSampleIncoming,
  readAllSampleIncoming,
  deleteSampleIncoming,
  downloadFile,
};
