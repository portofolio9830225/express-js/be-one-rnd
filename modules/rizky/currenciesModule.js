const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-checklist:payment-remark-module");
const helper = require("../../common/helper");
const db = require("../../models");
const CurrenciesModel = db.currencies;

async function readAllCurrencies(query) {
  //query string
  let condition = {
    is_active: true,
  };

    const sort = { name: 1 };

  const currenciesAll = await CurrenciesModel.find(condition).sort(sort).lean();
  if (helper.isEmptyObject(currenciesAll)) {
    return {
      foundData: [],
      currentPage: page,
      countPages: 0,
      countData: 0,
    };
  }
  let result = [];

    currenciesAll.forEach(async (_currenciess, index) => {
        let _currenciess_result = {
            value: _currenciess.value,
            text: _currenciess.value,
            label: _currenciess.value,
        };
        result.push(_currenciess_result);
    });

  return {
    countData: Object.keys(currenciesAll).length,
    foundData: result,
  };
}

module.exports = {
  readAllCurrencies,
};
