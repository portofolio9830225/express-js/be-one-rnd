const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-checklist:payment-remark-module");
const helper = require("../../common/helper");
const db = require("../../models");
const CategoiresModel = db.categories;

async function readAllCategories(query) {
  //query string
  let condition = {
    is_active: true,
  };

  const sort = { name: 1 };

  const categoriesAll = await CategoiresModel.find(condition).sort(sort).lean();
  if (helper.isEmptyObject(categoriesAll)) {
    return {
      foundData: [],
      currentPage: page,
      countPages: 0,
      countData: 0,
    };
  }
  let result = [];

  categoriesAll.forEach(async (_categoriess, index) => {
    let _categoriess_result = {
      value: _categoriess.name,
      text: _categoriess.name,
      label: _categoriess.name,
    };
    result.push(_categoriess_result);
  });

  return {
    countData: Object.keys(categoriesAll).length,
    foundData: result,
  };
}

async function createCategory(payload) {
  try {
    let data = {
      name: payload.name,
      is_active: true,
    };
    let result = await new CategoiresModel(data).save();
    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  readAllCategories,
  createCategory,
};
