const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-checklist:payment-remark-module");
const helper = require("../../common/helper");
const db = require("../../models");
const SampleIncomingRemark = db.sample_incoming_remark;

async function readAll(query) {
  //query string
  let condition = {
    is_active: true,
  };

    const sort = { name: 1 };

  const remarkAll = await SampleIncomingRemark.find(condition).sort(sort).lean();
  if (helper.isEmptyObject(remarkAll)) {
    return {
      foundData: [],
      currentPage: page,
      countPages: 0,
      countData: 0,
    };
  }
  let result = [];

    remarkAll.forEach(async (_remark, index) => {
        let _remark_result = {
            value: _remark.name,
            text: _remark.name,
            label: _remark.name,
        };
        result.push(_remark_result);
    });

  return {
    countData: Object.keys(remarkAll).length,
    foundData: result,
  };
}

module.exports = {
  readAll,
};
