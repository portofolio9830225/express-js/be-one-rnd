const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:registrasi-ticket-module");
const helper = require("../../common/helper");
const db = require("../../models");
const zeroPad = require("../../helpers/zeroPad");
const moment = require("moment");
const TicketModel = db.ticket;
const GeneralSettingsModel = db.general_settings;
const { ObjectId } = require("mongodb");
const { Query } = require("mongoose");
const CustomerModel = db.customers_ticket;
const dotenv = require("dotenv").config();
const FormData = require("form-data");
const path = require("path");
const { readFileSync, unlinkSync } = require("fs");
const axios = require("axios");
const MailModule = require("../mailModule");

const createSchema = Joi.object({
	target_price: Joi.string(),
	initial_project: Joi.string().required(),
	currency: Joi.string(),
	sample_product: Joi.string(),
	customer_name: Joi.string().required(),
	note: Joi.string().required(),
	category: Joi.string().required(),
	information: Joi.array().items(
		Joi.object({
			name: Joi.string(),
			value: Joi.string(),
		})
	),
});

const updateSchema = Joi.object({
	_id: Joi.string().required(),
	name: Joi.string().required(),
	details: Joi.array().items(
		Joi.object({
			name: Joi.string().required(),
		})
	),
});

const idSchema = Joi.object({
	_id: Joi.string().required(),
});
function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
	return updateSchema.validate(schema);
}
function validateIdSchema(schema) {
	return idSchema.validate(schema);
}

async function createTicket(payload_params, currentUser) {
	let payload = payload_params;

	const dateNow = new Date();
	let currentMonth = dateNow.getMonth() + 1;
	let currentYear = dateNow.getFullYear();
	let query = {};

	let getDataTicket = await TicketModel.find(query);
	let urutan = 0;
	if (!helper.isEmptyArray(getDataTicket)) {
		urutan = getDataTicket.length;
	}

	let docNumber = `RND/${currentYear}/${currentMonth}/${moment().format("DD")}/${zeroPad.zeroPad(urutan + 1, 6)}`;

	payload.sample_product = true;
	payload.history = [
		{
			requester: currentUser.username,
			date: new Date(),
			data: [],
			status: "Dokumen Dibuat",
			status_code: "dokumen_dibuat",
			remark: "Create Ticket",
		},
	];

	let filter = {
		name: `${payload.customer_name}`,
	};

	let customer = await CustomerModel.findOne(filter);

	payload.is_active = true;
	payload.to = "";
	payload.document_number = docNumber;
	payload.target_price = `${payload.target_price}`;
	payload.currency = `${payload.currency}`;
	payload.customer_name = `${customer.name}`;
	payload.customer_code = `${customer.name}`;
	payload.status = "Dokumen Dibuat";
	payload.status_code = "dokumen_dibuat";
	payload.pic = currentUser.username;
	payload.pic_name = currentUser.person_name;
	payload.category = `${payload.category}`;
	payload.date = new Date();
	payload.target_date = payload.target_date;
	payload.created_at = new Date();
	payload.updated_at = new Date();
	payload.created_by = currentUser.username;
	payload.updated_by = currentUser.username;

	const ticket = await new TicketModel(payload).save();
	const user = await GeneralSettingsModel.findOne({ key: "mail_head_rnd" });
	let information = "";
	payload.information.forEach(async e => {
		information += `<tr><td>${e.name}</td><td>${e.value}</td></tr>`;
	});

	let sample = "";
	payload.sample.forEach(async e => {
		sample += `<tr><td>${e.sampel_name}</td><td>${e.sampel_code}</td><td>${e.batch}</td><td>${e.qty}</td><td>${e.uom}</td></tr>`;
	});
	const body = `<span class="preheader">&nbsp;&nbsp;&nbsp;</span> <p>Hi, </p><p>This is information of request development product :</p><br><table role="presentation" border="1" cellpadding="2" cellspacing="0" id="tabel_set"> <tbody> <tr> <td>Customer</td><td>${
		ticket.customer_name
	}</td></tr><tr> <td>Traget</td><td>${ticket.category}</td></tr><tr> <td>Note</td><td>${
		ticket.note
	}</td></tr><tr> <td>Initial Project</td><td>${ticket.initial_project}</td></tr><tr> <td>Target Date</td><td>${
		ticket.target_date
	}</td></tr><tr> <td>Date of Communication</td><td>${
		ticket.date_of_communication
	}</td></tr><tr> <td>Target Price</td><td>${
		ticket.target_price ? ticket.target_price : "-"
	}</td></tr><tr> <td>Currency</td><td>${
		ticket.currency
	}</td></tr></tbody> </table><h4>Additional Information</h4><table role="presentation" border="1" cellpadding="2" cellspacing="0" id="tabel_set"> <tbody><tr> <th>Information</th> <th>Value</th></tr> ${information}</tbody> </table><h4>Sample</h4><table role="presentation" border="1" cellpadding="2" cellspacing="0" id="tabel_set"> <tbody> <tr> <th>Sample Code</th> <th>Sample Name</th> <th>Batch</th> <th>Qty</th> <th>Uom</th> ${sample}</tbody> </table>`;
	const send = {
		subject: "Request Development Product",
		body: body,
		send_to: [{ name: "-", address: `${user.value}` }],
		send_cc: [],
	};
	const resultMail = await MailModule.sendEmail(send.subject, send.body, send.send_to, send.send_cc);

	return { data: ticket, status: "success " };
}

async function upload(payload, currentUser) {
	debug(currentUser, " => payload");

	const filter = { is_active: true, _id: payload.id };
	const ticket = await TicketModel.findOne(filter).lean();
	let update = ticket;
	const { history } = ticket;

	let parameters = {
		method: "POST",
		url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
		headers: {
			"x-application-token": `${payload.app_token}`,
			"x-public-key": `${process.env.PUBLIC_KEY}`,
		},
	};

	const form = new FormData();
	form.append("apps", process.env.NAME);

	//upload files to onedrive, get the link
	payload.files.forEach(f => {
		const readDir = path.join(__dirname, `../../${f.path}`);
		const readFile = readFileSync(readDir);
		form.append("upload_file", readFile, f.path);
	});
	parameters.data = form;
	parameters.maxContentLength = Infinity;
	parameters.maxBodyLength = Infinity;
	parameters.headers["Content-Type"] = "multipart/form-data;boundary=" + form.getBoundary();

	const result = await axios(parameters)
		.then(function async(response) {
			return response.data;
		})
		.catch(function (error) {
			debug(error, "=> error callApiByArea");
			return [];
		});
	debug(result, " result upload file");

	const OneDriveId = result[0].onedrive_id;

	for (let f of payload.files) {
		// remove files when finish make call
		unlinkSync(path.join(__dirname, `../../${f.path}`));
	}

	history.push({
		requester: currentUser.username,
		data: [{ document: OneDriveId }],
		status: "Sedang Analisa Sample",
		status_code: "sedanga_analiasa_sample",
		remark: "",
	});
	update.status = "Sedang Analisa Sample";
	update.status_code = "sedanga_analiasa_sample";
	update.history = history;

	update.update_at = new Date();

	let resultTicket = await TicketModel.findOneAndUpdate(filter, update, {
		useFindAndModify: false,
		new: true,
	});

	return { data: resultTicket, status: "Data has been upload successfully" };
}

async function uploadSelesaiAnalisa(payload, currentUser) {
	debug(currentUser, " => payload");

	const filter = { is_active: true, _id: payload.id };
	const ticket = await TicketModel.findOne(filter).lean();
	let update = ticket;
	const { history } = ticket;

	let parameters = {
		method: "POST",
		url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
		headers: {
			"x-application-token": `${payload.app_token}`,
			"x-public-key": `${process.env.PUBLIC_KEY}`,
		},
	};

	const form = new FormData();
	form.append("apps", process.env.NAME);

	//upload files to onedrive, get the link
	payload.files.forEach(f => {
		const readDir = path.join(__dirname, `../../${f.path}`);
		const readFile = readFileSync(readDir);
		form.append("upload_file", readFile, f.path);
	});
	parameters.data = form;
	parameters.maxContentLength = Infinity;
	parameters.maxBodyLength = Infinity;
	parameters.headers["Content-Type"] = "multipart/form-data;boundary=" + form.getBoundary();

	const result = await axios(parameters)
		.then(function async(response) {
			return response.data;
		})
		.catch(function (error) {
			debug(error, "=> error callApiByArea");
			return [];
		});
	debug(result, " result upload file");

	const OneDriveId = result[0].onedrive_id;

	for (let f of payload.files) {
		// remove files when finish make call
		unlinkSync(path.join(__dirname, `../../${f.path}`));
	}

	history.push({
		requester: currentUser.username,
		data: [{ document: OneDriveId }],
		status: "Siap Formulasi",
		status_code: "siap_formulasi",
		remark: "",
	});
	update.status = "Siap Formulasi";
	update.status_code = "siap_formulasi";
	update.history = history;

	update.update_at = new Date();

	let resultTicket = await TicketModel.findOneAndUpdate(filter, update, {
		useFindAndModify: false,
		new: true,
	});

	return { data: resultTicket, status: "Data has been upload successfully" };
}

async function readAllTicket(query) {
	//query string

	const dateNow = new Date();
	let currentMonth = dateNow.getMonth() + 1;
	let currentYear = dateNow.getFullYear();
	let condition = {
		is_active: true,
	};
	debug(new Date(currentYear, currentMonth - 1).toISOString(), ">>> this is date");

	Object.entries(query).forEach(([key, val]) => {
		debug(val, ">> ", key);
		if (key.indexOf("search_") > -1) {
			const column = key.replace("search_", "");
			condition[column] = val;
		} else if (key.indexOf("searchdate_target_date") > -1) {
			/**
			 * example for frontend
			 * searchdate_created_at: JSON.stringify({
			 * 		$gte: moment(state.dateRange.startDate).format('YYYY-MM-DD'),
			 * 		$lte: moment(state.dateRange.endDate).format('YYYY-MM-DD'),
			 * 	})
			 * @type {string}
			 */
			const column = key.replace("searchdate_target_date", "target_date");
			const newVal = JSON.parse(val);

			if (typeof newVal === "object") {
				let temp = {};
				Object.entries(newVal).forEach(([key2, val2]) => {
					temp[key2] = new Date(val2).toISOString();
				});
				condition[column] = temp;
			} else if (typeof newVal === "string") {
				condition[column] = new Date(newVal);
			}
		} else if (key.indexOf("filter_") > -1) {
			const column = key.replace("filter_", "");
			const query_like = { $regex: val, $options: "-i" };

			if (column === "ticket_id") {
				condition["document_number"] = query_like;
			} else if (column === "pic") {
				condition["pic_name"] = query_like;
			} else if (column === "sample") {
				condition["sample_product"] = query_like;
			} else {
				condition[column] = query_like;
			}
		}
	});

	if (query.start && query.end) {
		condition.target_date = {
			$gte: moment(query.start).format("YYYY-MM-DD"),
			$lte: moment(query.end).format("YYYY-MM-DD"),
		};
	}

	debug(condition);
	//sort
	const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
	const sortField = query.sortBy ? query.sortBy : "date";
	const sort = query.sortBy
		? {
				[sortField]: sortOrder,
		  }
		: {
				date: -1,
		  };
	//skip
	const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
	const page = query.page ? Number(query.page) : 1;
	const skip = sizePerPage * page - sizePerPage;
	//limit
	const limit = sizePerPage;
	const ticket = await TicketModel.find(condition)
		.sort(sort)
		.skip(skip)
		.limit(limit)
		.select("-__v -created_at -updated_at -is_deleted")
		.lean();
	const ticketAll = await TicketModel.find(condition).lean();
	if (helper.isEmptyObject(ticket)) {
		return {
			foundData: [],
			currentPage: page,
			countPages: 0,
			countData: 0,
		};
	}
	let result = [];

	ticket.forEach(async (_tickets, index) => {
		let _tickets_result = {
			no: index + 1,
			date: _tickets.date,
			document_number: _tickets.document_number,
			sample_product: _tickets.sample ? (_tickets.sample.length > 0 ? "Yes" : "No") : "No",
			customer_name: _tickets.customer_name,
			customer_code: _tickets.customer_code,
			currency: _tickets.currency,
			target_price: _tickets.target_price,
			target_date: _tickets.target_date,
			category: _tickets.category,
			currency: _tickets.currency,
			information: _tickets.information,
			initial_project: _tickets.initial_project,
			sample: _tickets.sample ? (_tickets.sample.length > 0 ? _tickets.sample : []) : [],
			target_date: _tickets.target_date,
			date_of_communication: _tickets.date_of_communication,
			history: _tickets.history,
			status_code: _tickets.status_code,
			pic: _tickets.pic,
			pic_name: _tickets.pic_name,
			note: _tickets.note,
			_id: _tickets._id,
		};
		result.push(_tickets_result);
	});

	return {
		foundData: result,
		currentPage: page,
		countPages: Math.ceil(Object.keys(ticketAll).length / sizePerPage),
		countData: Object.keys(ticketAll).length,
	};
}

async function updateRegistrasiTicket(payload) {
	const filter = { _id: ObjectId(payload._id) };
	let update = payload;
	update.sample_product = payload.sample_product === "No" ? false : true;

	let filterCustomer = {
		name: `${payload.customer_name}`,
	};

	let customer = await CustomerModel.findOne(filterCustomer);
	payload.customer_name = `${customer.name}`;
	payload.customer_code = `${customer.name}`;
	update.update_at = new Date();

	let invoiceCategory = await TicketModel.findOneAndUpdate(filter, update, {
		useFindAndModify: false,
		new: true,
	});

	if (helper.isEmptyObject(invoiceCategory)) {
		return error.errorReturn({ message: "Ticket not found" });
	}

	return {
		data: invoiceCategory,
		status: "Data has been updated successfully",
	};
}

async function deleteTicket(payload) {
	const validate = validateIdSchema(payload);
	if (validate.error) {
		debug(validate.error);
		return error.errorReturn({ message: validate.error.details[0].message });
	}
	const filter = { _id: payload._id };
	const update = {
		updated_at: new Date(),
		is_active: false,
	};
	let ticketDelete = await TicketModel.findOneAndUpdate(filter, update, {
		useFindAndModify: false,
		new: true,
	});
	if (helper.isEmptyObject(ticketDelete)) {
		return error.errorReturn({ message: "Menu not found" });
	}

	return { data: ticketDelete, status: "Data has been deleted successfully" };
}

async function prosesForumulasi(payload) {
	const validate = validateIdSchema(payload.data);
	if (validate.error) {
		debug(validate.error);
		return error.errorReturn({ message: validate.error.details[0].message });
	}
	const filter = { _id: payload.data._id };
	const update = {
		updated_at: new Date(),
		status: "Siap Formulasi",
		status_code: "siap_formulasi",
	};
	let ticketDelete = await TicketModel.findOneAndUpdate(filter, update, {
		useFindAndModify: false,
		new: true,
	});
	if (helper.isEmptyObject(ticketDelete)) {
		return error.errorReturn({ message: "Menu not found" });
	}

	return { data: ticketDelete, status: "Successfully" };
}

async function readByIdTicket(payload) {
	const filter = { is_active: true };
	Object.entries(payload).forEach(([key, val]) => {
		filter.document_number = key;
	});

	const ticket = await TicketModel.findOne(filter).lean();

	if (helper.isEmptyObject(ticket)) {
		return error.errorReturn({ message: "Ticket not found" });
	}

	return ticket;
}

module.exports = {
	createTicket,
	readAllTicket,
	updateRegistrasiTicket,
	deleteTicket,
	upload,
	readByIdTicket,
	uploadSelesaiAnalisa,
	prosesForumulasi,
};
