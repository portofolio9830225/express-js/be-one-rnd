const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-checklist:payment-remark-module");
const helper = require("../../common/helper");
const db = require("../../models");
const AdditionalInformationModel = db.additional_information;

async function readAllAdditionalInformation() {
    //query string
    let condition = {
        is_active: true,
        category: 'register_sample'
    };

    const additional_informationAll = await AdditionalInformationModel.distinct('name', condition).lean();
    if (helper.isEmptyObject(additional_informationAll)) {
        return {
            foundData: [],
            currentPage: page,
            countPages: 0,
            countData: 0,
        };
    }
    let result = [];

    additional_informationAll.forEach(async (_additional_informations, index) => {
        let _additional_informations_result = {
            value: _additional_informations,
            text: _additional_informations,
            label: _additional_informations,
        };
        result.push(_additional_informations_result);
    });

    return {
        countData: Object.keys(additional_informationAll).length,
        foundData: result,
    };
}

module.exports = {
    readAllAdditionalInformation
}