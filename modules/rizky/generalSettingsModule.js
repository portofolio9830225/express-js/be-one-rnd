
async function emailNotification(payload) {
    const jwt_token = jwt.sign(
        {
            name: process.env.NAME,
            type: "private_apps",
        },
        process.env.PRIVATE_KEY,
        { expiresIn: "2400h" }
    );

    const html = JSON.stringify(payload);
    const body = {
        name: "Coin",
        "send_to": [{ "name": "-", "address": "mis.stf05@lnk.co.id" }],
        "send_cc": [],
        category: "mail",
        body_type: "html",
        subject: "One RND - Final Report ${jn_number}",
        body: html,
        sending_type: "now",
        sending_time: dateFormat(date_now, "yyyy-mm-dd HH:ss"),
        sending_day: "",
        sending_date: "",
        periode_start: dateFormat(date_now, "yyyy-mm-dd HH:ss"),
        periode_end: dateFormat(date_now, "yyyy-mm-dd HH:ss"),
    };

    const parameters = {
        method: "POST",
        url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/alert/`,
        headers: {
            "X-Public-Key": process.env.PUBLIC_KEY,
            "X-Application-Token": `Bearer ${jwt_token}`,
        },
        data: body,
    };

    const result = await axios(parameters);

    return {
        result
    }
}

module.exports = {
    emailNotification,
}