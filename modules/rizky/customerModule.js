const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-checklist:payment-remark-module");
const helper = require("../../common/helper");
const db = require("../../models");
const CustomerModel = db.customer;

async function readAllCustomers(query) {
    //query string
    let condition = {
        is_active: true,
    };

    const customerAll = await CustomerModel.find(condition).lean();
    if (helper.isEmptyObject(customerAll)) {
        return {
            foundData: [],
            currentPage: page,
            countPages: 0,
            countData: 0,
        };
    }
    let result = [];

    customerAll.forEach(async (_customers, index) => {
        let _customers_result = {
            value: _customers.code,
            text: _customers.name,
            label: _customers.code +' - '+ _customers.name
        };
        result.push(_customers_result);
    });

    return {
        countData: Object.keys(customerAll).length,
        foundData: result,
    };
}

module.exports = {
    readAllCustomers
}