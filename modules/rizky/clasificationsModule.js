const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-checklist:payment-remark-module");
const helper = require("../../common/helper");
const db = require("../../models");
const ClasificationModel = db.clasification;

async function list(query) {
  //query string
  let condition = {
    is_active: true,
  };

const sort = { name: 1 };

  const clasificationsAll = await ClasificationModel.find(condition).sort(sort).lean();
  if (helper.isEmptyObject(clasificationsAll)) {
    return {
      foundData: [],
      currentPage: page,
      countPages: 0,
      countData: 0,
    };
  }
  let result = [];

    clasificationsAll.forEach(async (_clasificationss, index) => {
        let _clasificationss_result = {
            value: _clasificationss.name,
            text: _clasificationss.name,
            label: _clasificationss.name,
        };
        result.push(_clasificationss_result);
    });

  return {
    countData: Object.keys(clasificationsAll).length,
    foundData: result,
  };
}

module.exports = {
  list,
};
