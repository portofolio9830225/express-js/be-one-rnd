const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:ticket-module");
const db = require("../models");
const helper = require("../common/helper");
const mongoose = require("mongoose");
const Tickets = db.ticket_f;
const Formulas = db.formula_f;
const axios = require("axios");
const URL_FILE_SERVICE = "/api/file/onedrive/read-file/";
const moment = require("moment");

const createSchema = Joi.object({});

const updateSchema = Joi.object({});

const deleteSchema = Joi.object({});

function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
	return updateSchema.validate(schema);
}
function validateDeleteSchema(schema) {
	return deleteSchema.validate(schema);
}

async function create(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD CREATE TICKET");

		const validate = validateCreateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR CREATE TICKET");
		throw error.errorReturn({ message: "Internal Server Error" });
	}
}

async function read(query) {
	try {
		debug(query);
		debug("===> QUERY READ TICKET");

		// query
		let filter = { is_active: true };

		// sort
		const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query?.sortField ?? "created_at";
		const sort = { [sortField]: sortOrder };

		// skip
		const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
		const page = query?.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		// limit
		const limit = sizePerPage;

		// find data
		let found_data = [];

		if (query?.showAll) {
			found_data = await Tickets.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.select("-__v")
				.lean();
		} else {
			found_data = await Tickets.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.skip(skip)
				.limit(limit)
				.select("-__v")
				.lean();
		}

		// if empty data
		if (helper.isEmptyArray(found_data)) {
			const result = {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
			debug(result);
			debug("===> RESULT READ FORMULA");
			return result;
		}

		//
		const count_data = await Tickets.countDocuments(filter);
		const current_page = query.showAll ? 1 : page;
		const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
		const result = {
			foundData: found_data,
			currentPage: current_page,
			countPages: count_page,
			countData: count_data,
		};

		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR READ TICKET");
		throw error.errorReturn({ message: "Internal Server Error" });
	}
}

async function readById(query, app_token) {
	try {
		debug(query);
		debug("===> QUERY READ TICKET");

		const result = await Tickets.findById(query.id);

		if (helper.isEmptyObject(result)) {
			return error.errorReturn({ message: "Ticket not found" });
		}

		const master_history = result.history;

		let filter_formula = {
			is_active: true,
			ticket_id: mongoose.Types.ObjectId(query.id),
		};
		if (query.ticket_id) {
			filter_formula._id = mongoose.Types.ObjectId(query.ticket_id);
		}
		const formula = await Formulas.find(filter_formula).lean();

		let promises = [];
		let master_formula = [];

		formula.map(x => {
			x.history.map(y => {
				if (y.data && y.data.length > 0) {
					y.data.map(z => {
						if (z.document) {
							const parameters = {
								method: "GET",
								url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}${URL_FILE_SERVICE}?onedrive_id=${z.document}`,
								headers: {
									"x-application-token": `${app_token}`,
									"x-public-key": `${process.env.PUBLIC_KEY}`,
								},
							};
							promises.push(
								axios(parameters)
									.then(res => {
										let data = res.data;
										data.document = z.document;
										master_formula.push(data);
									})
									.catch(e => {
										debug(e);
									})
							);
						}
					});
				}
			});
		});

		await Promise.all(promises).then(() => {
			debug("===> DONE LOAD PROMISE");
		});
		let new_master_formula = [].concat(...master_formula);

		let new_formula = formula.map(x => {
			let ini_history = x.history.map(y => {
				if (y.data && y.data.length > 0) {
					let ini_data = y.data.map(z => {
						if (z.document) {
							let hasil_ = new_master_formula.find(function (post) {
								if (post.document === z.document) return true;
							});

							if (typeof hasil_ != "undefined") {
								return { ...hasil_ };
							}
							return { ...z };
						}
						return { ...z };
					});
					return { ...y, data: ini_data };
				}
				return { ...y };
			});
			let new_history = [...master_history, ...ini_history];

			const new_data = [];
			new_history.map(e => {
				const dt = { ...e };
				dt.date = new Date(moment(e.date).format("YYYY-MM-DD HH:mm:ss"));
				return new_data.push(dt);
			});

			const sortDate = new_data.sort((a, b) => a.date - b.date);

			debug("==> HISTORY");
			debug(sortDate);

			return { ...x, history: sortDate };
		});

		result.formula = new_formula;

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR READ TICKET");
		throw error.errorRetuyrn({ message: "Internal Server Error" });
	}
}

async function update(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD UPDATE TICKET");

		const validate = validateUpdateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR UPDATE TICKET");
		throw error.errorReturn({ message: "Internal Server Error" });
	}
}

async function destroy(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD DELETE TICKET");

		const validate = validateDeleteSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR DELETE TICKET");
		throw error.errorReturn({ message: "Internal Server Error" });
	}
}

module.exports = { create, read, readById, update, destroy };
