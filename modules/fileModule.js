const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:file-module");
const db = require("../models");
const helper = require("../common/helper");
const File = db.file;
const axios = require("axios");
const FormData = require("form-data");
const { readFileSync } = require("fs");
const PATH = require("path");

const createSchema = Joi.object({
  files: Joi.array().required(),
  app_token: Joi.string().required(),
  department: Joi.string().required(),
});

const uploadSchema = Joi.object({
  files: Joi.array().required(),
  app_token: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateUploadSchema(schema) {
  return uploadSchema.validate(schema);
}

async function create(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE FILE");

    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    if (payload.files.length > 0) {
      // init
      const END_POINT = "api/file/onedrive/upload-file/";
      const GATEWAY_IP = process.env.GATEWAY_IP;
      const GATEWAY_PORT = process.env.GATEWAY_PORT;
      const PUBLIC_KEY = process.env.PUBLIC_KEY;

      // init file
      const formData = new FormData();
      formData.append("apps", process.env.NAME);

      // load file
      payload.files.forEach((file) => {
        const readDir = PATH.join(__dirname, `/../${file.path}`);
        const readFile = readFileSync(readDir);
        formData.append("upload_file", readFile, file.path);
      });

      // init parameter
      const parameter = {
        method: "POST",
        url: `${GATEWAY_IP}:${GATEWAY_PORT}/${END_POINT}`,
        headers: {
          "x-application-token": `${payload.app_token}`,
          "x-public-key": PUBLIC_KEY,
          "Content-Type": `multipart/form-data;boundary=${formData.getBoundary()}`,
        },
        data: formData,
        maxContentLength: Infinity,
        maxBodyLength: Infinity,
      };
      //
      const result_upload = await axios(parameter);

      debug(result_upload);
      debug("===> RESULT CREATE FILE");

      if (result_upload?.error) {
        return error.errorReturn({ message: "Upload File Failed" });
      }
      return result_upload.data;
    } else {
      return error.errorReturn({ message: "Upload File Failed" });
    }
  } catch (e) {
    debug(e);
    debug("===> ERROR CREATE FILE");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = { create };
