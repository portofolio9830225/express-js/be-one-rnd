const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:customers-module");
const db = require("../../models");
const moment = require("moment");
const FormulaModel = db.formula_f;

async function readAll(query) {
	try {
		debug(query);
		let condition = {};
		condition.job_number = { $nin: [null, ""] };
		if (query.sensory === 'true') {
			condition.sensory = {$exists: true, $type: 'array', $ne: [] };
		}

		switch (query.category) {
			case "Job Number":
				condition.job_number = { $nin: [null, ""], $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			case "Customer":
				condition.customer_name = { $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			case "PIC":
				condition.pic_name = { $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			case "Ticket ID":
				condition.document_number = { $regex: ".*" + query.keyword + ".*", $options: "i" };
				break;
			default:
				break;
		}

		if (query.start && query.end) {
			condition.created_at = {
				$gte: moment(query.start).startOf("day"),
				$lte: moment(query.end).startOf("day"),
			};
		}

		debug(condition);

		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		const data = await FormulaModel.find(condition).skip(skip).limit(sizePerPage);
		const numFound = await FormulaModel.find(condition);

		return {
			data: data,
			countData: Object.keys(numFound).length,
			currentPage: page,
			countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
		};
	} catch (err) {
		debug(err);
		throw error.errorReturn({ message: "Internal server error" });
	}
}

module.exports = {
	readAll,
};
