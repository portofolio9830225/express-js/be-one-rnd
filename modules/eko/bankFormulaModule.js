const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:formula-module");
const mongoose = require("mongoose");
const helper = require("../../common/helper");
const db = require("../../models");
const moment = require("moment");
const Formula_F = db.formula_e;
const zeroPad = require("../../helpers/zeroPad");

const formulasiSchema = Joi.object({
  _id: Joi.string(),
  requester: Joi.string().required(),
  formula: Joi.array().required(),
  fg_name: Joi.string(),
  fg_code: Joi.string(),
  person_name: Joi.string(),
});

function validateFormulasi(schema) {
  return formulasiSchema.validate(schema);
}

async function read(query) {
  try {
    let condition = {};
    condition.is_active = true;
    condition.status_code = {
      $in: ["data_master"],
    };
    if (query.result_fg_name) {
      condition.result_fg_name = { $regex : query.result_fg_name };
    }

    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "date";
    const sort = query.sortBy
      ? {
        [sortField]: sortOrder,
      }
      : {
        date: 1,
      };

    //skip
    const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;
    //limit
    const limit = sizePerPage;

    const ck = await Formula_F.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(ck)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await Formula_F.find(condition);
    const result = {
      foundData: ck,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function formulasi(payload) {
  try {
    debug(payload);
    const validate = validateFormulasi(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    // payload.formula.forEach(async (elm) => {
    for (elm of payload.formula) {
      if (elm.editable != false) {
        //generate queue number
        let new_date = new Date();
        let getOldData = await Formula_F.find({
          created_at: {
            $gte: moment(new_date).startOf("date"),
          },
          queue_number: { $exists: true },
        }).exec();

        urutan = getOldData.length + 1;
        debug(urutan, " urutan");

        if (elm._id) {
          debug("EDIT =>");
          let rowData = await Formula_F.findOne({_id : elm._id}).lean();
          
          let formula = {
            job_number: null,
            queue_number: elm.queue_number
              ? elm.queue_number
              : moment(new Date()).format("YYYY-MM-DD") +
              "-" +
              zeroPad.zeroPad(urutan, 3),
            sample_product: rowData.sample_product,
            customer_code: rowData.customer_code,
            customer_name: rowData.customer_name,
            document_number: rowData.document_number,
            target_date: new Date(rowData.target_date),
            editable: true,
            status: rowData.status_name,
            status_code: rowData.status_code,
            result_fg_name: payload.fg_name,
            result_fg_code: payload.fg_code,
            created_by: payload.requester,
            created_at: new_date,
            is_active: true,
            history: [],
            due_date_miniplant: rowData.due_date_miniplant,
            formula: elm.formula,
            details: elm.details,
            pic: payload.requester,
            pic_name: payload.person_name,
            pic_ticket: rowData.pic,
            pic_ticket_name: rowData.pic_name,ticket_id: mongoose.Types.ObjectId(rowData._id),
          };
          let history = [
            {
              requester: payload.requester,
              data: null,
              status: rowData.status_name,
              status_code: rowData.status_code,
              remark: "",
              date: new Date(),
            },
          ];

          const getFormula = await Formula_F.findOne({
            is_active: true,
            _id: mongoose.Types.ObjectId(elm._id),
          });
          if (!helper.isEmptyObject(getFormula)) {
            //update here
            formula.history = [...getFormula.history, ...history];
            let _update = await Formula_F.findOneAndUpdate(
              { _id: mongoose.Types.ObjectId(elm._id) },
              formula,
              {
                useFindAndModify: false,
                new: true,
              }
            );
          }
        } else {
          debug("INSERT =>");
          //insert here
          let formula = {
            job_number: null,
            queue_number: elm.queue_number
              ? elm.queue_number
              : moment(new Date()).format("YYYY-MM-DD") +
              "-" +
              zeroPad.zeroPad(urutan, 3),
            sample_product: null,
            customer_code: null,
            customer_name: null,
            document_number: null,
            target_date: null,
            editable: true,
            status: "Data Master",
            status_code: "data_master",
            result_fg_name: payload.fg_name,
            result_fg_code: payload.fg_code,
            created_by: payload.requester,
            created_at: new_date,
            is_active: true,
            history: [],
            due_date_miniplant: null,
            formula: elm.formula,
            details: elm.details,
            pic: payload.requester,
            pic_name: payload.person_name,
            pic_ticket: null,
            pic_ticket_name: null,ticket_id: "",
          };
          formula.history = [];
          let _insert = await new Formula_F(formula).save();
        }
        debug("end of loop");
      }
    }
    // });

    return {};
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function readAllFormula(query) {
  try {
    const getFormula = await Formula_F.find({
      is_active: true,
      _id: query.id,
    }).lean();

    if (helper.isEmptyObject(getFormula))
      return error.errorReturn({
        message: "Document is not found, please reload your page",
      });

    debug(getFormula, 'FORMULA =>')

    let detailFOrmula = getFormula.map((x, index) => {
      return {
        ...x,
        title: x.job_number ? x.job_number : "New Formula",
        due_date_miniplant: moment(x.due_date_miniplant).format("YYYY-MM-DD"),
        fg_code: x.result_fg_code,
        fg_name: x.result_fg_name,
        index: index,
      };
    });

    return detailFOrmula;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function deleteFormula(payload) {
  try {
    debug(payload, " => payload");
    const getFormula = await Formula_F.find({
      is_active: true,
      editable: true,
      _id: mongoose.Types.ObjectId(payload._id),
    }).lean();

    if (helper.isEmptyObject(getFormula)) {
      return error.errorReturn({
        message: "Document can't delete or not found",
      });
    }

    let update = {};
    update.is_active = false;
    update.updated_at = new Date();
    update.updated_by = payload.requester;
    await Formula_F.findOneAndUpdate(
      { _id: mongoose.Types.ObjectId(payload._id) },
      update,
      {
        useFindAndModify: false,
        new: true,
      }
    );

    return {};
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}


module.exports = {
  read,
  formulasi,
  readAllFormula,
  deleteFormula
};
