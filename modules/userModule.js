require("dotenv").config();
const debug = require("debug")("backend-one-rnd:user-modules");
const Joi = require("joi");
const jwt = require("jsonwebtoken");
const axios = require("axios");
const helper = require("../common/helper");
const db = require("../models");
const Authentication = db.authentication;
const AuthenticationHistory = db.authentication_history;
const GeneralSeting = db.general_settings;
const Roles = db.roles;
const Segment = db.segment;
const error = require("../common/errorMessage");
const Encryptor = require("../modules/Encryptor");
const escapeRegex = require("../common/escapeRegex");
const { result } = require("lodash");

const createSchema = Joi.object({
  requester: Joi.string().required(),
  department: Joi.string().required(),
  username: Joi.string().required(),
  name: Joi.string().required(),
  email: Joi.string().required(),
  type: Joi.string().required(),
  detail_user: Joi.object().required(),
  role: Joi.array().required(),
  segment_code: Joi.string().required(),
});
const updateSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  department: Joi.string().required(),
  username: Joi.string().required(),
  name: Joi.string().required(),
  email: Joi.string().required(),
  type: Joi.string().required(),
  detail_user: Joi.object().required(),
  role: Joi.array().required(),
  segment_code: Joi.string().required(),
});
const deleteSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}
function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}

// Get All Department
const getAllDepartment = async (payload) => {
  try {
    debug(payload, "=> params");
    let params = {
      access_token: payload.access_token,
    };

    const jwt_token = jwt.sign(
      {
        name: process.env.NAME,
        type: "private_apps",
      },
      process.env.PRIVATE_KEY,
      {
        expiresIn: "2400h",
      }
    );

    const body = {};
    let url = `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/user/role/`;
    const parameters = {
      method: "POST",
      url: url,
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `Bearer ${params.access_token}`,
      },
      data: body,
    };

    const result_all_role = await axios(parameters);

    const result_department = result_all_role.data.map(
      (value) => value.role.split(" ")[0]
    );
    const result_department_filter = result_department.filter(
      (value, index, self) => {
        return self.indexOf(value) === index;
      }
    );

    return result_department_filter;
  } catch (err) {
    debug(err);
    return err.response.data;
  }
};

// Get Send Notif User
const getSendNotifUser = async () => {
  try {
    const get_user = await GeneralSeting.findOne({
      key:"mail_notification_register_sample",
      is_active:true}).lean();

      if (helper.isEmptyObject(get_user)) {
        return [];
      }
      return get_user;
  } catch (err) {
      return err;
  }
};

// Get Data User by Department
const getUserByDept = async (payload) => {
  try {
    debug(payload, "=> params");

    if (!payload.department) {
      return error.errorReturn({ message: "Required department" });
    }

    let params = {
      department: payload.department,
      access_token: payload.access_token,
    };

    const jwt_token = jwt.sign(
      {
        name: process.env.NAME,
        type: "private_apps",
      },
      process.env.PRIVATE_KEY,
      {
        expiresIn: "2400h",
      }
    );

    const body = {
      department: payload.department,
    };
    let url = `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/user/get-user-by-dept-div-pos/?dept=${params.department}`;
    const parameters = {
      method: "POST",
      url: url,
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `Bearer ${params.access_token}`,
      },
      data: body,
    };

    const result = await axios(parameters);

    const result_sort = result.data.sort((a, b) => {
      if (a.username < b.username) return -1;
      if (a.username > b.username) return 1;
      return 0;
    });

    return result_sort;
  } catch (err) {
    debug(err.response);
    return err.response.data;
  }
};

const getUserByDeptwithFilter = async (payload) => {
  try {
    debug(payload, "=> params");

    if (!payload.department) {
      return error.errorReturn({ message: "Required department" });
    }

    let params = {
      department: payload.department,
      access_token: payload.access_token,
    };

    const jwt_token = jwt.sign(
      {
        name: process.env.NAME,
        type: "private_apps",
      },
      process.env.PRIVATE_KEY,
      {
        expiresIn: "2400h",
      }
    );

    const body = {
      department: payload.department,
    };
    let url = `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/user/get-user-by-dept-div-pos/?dept=${params.department}`;
    const parameters = {
      method: "POST",
      url: url,
      headers: {
        "X-Public-Key": process.env.PUBLIC_KEY,
        "X-Application-Token": `Bearer ${jwt_token}`,
        "X-User-Token": `Bearer ${params.access_token}`,
      },
      data: body,
    };

    const result = await axios(parameters);

    const allow_nik = await GeneralSeting.findOne({
      is_active: true,
      key: "allow_pic_sample_incoming",
    }).lean();

    let new_data = result.data;
    if (!helper.isEmptyObject(allow_nik)) {
      new_data = new_data.filter(function (item) {
        if (allow_nik.value.includes(item.username)) {
          return item;
        }
      });
    }

    const result_sort = new_data.sort((a, b) => {
      if (a.username < b.username) return -1;
      if (a.username > b.username) return 1;
      return 0;
    });

    return result_sort;
  } catch (err) {
    debug(err.response);
    return err.response;
  }
};

const getDownline = async (query) => {
  try {
    let get_auth = await Authentication.findOne({
      is_active: true,
      username: query.requester,
    });
    if (helper.isEmptyObject(get_auth)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }

    let array_username = await searchDownline(
      query.segment_code,
      query.requester
    );

    let filter = {
      is_active: true,
      username: { $in: array_username },
    };

    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "username";
    const sort = query.sortBy
      ? {
          [sortField]: sortOrder,
        }
      : {
          created_at: -1,
        };

    //skip
    const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;
    //limit
    const limit = sizePerPage;

    const ck = await Authentication.find(filter)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyArray(ck)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await Authentication.find(filter);
    const result = {
      foundData: ck,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };
    return result;
  } catch (err) {
    debug(err);
    return err;
  }
};

const getDownlineSelect = async (query) => {
  try {
    let get_auth = await Authentication.findOne({
      is_active: true,
      username: query.username,
    });

    if (helper.isEmptyObject(get_auth)) {
      return [];
    }

    let array_username = await searchDownline(get_auth, query.username);
    let filter = {
      is_active: true,
      username: { $in: array_username },
    };

    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "username";
    const sort = query.sortBy
      ? {
          [sortField]: sortOrder,
        }
      : {
          created_at: -1,
        };

    const ck = await Authentication.find(filter)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .select("-__v");

    let result = ck.map((x) => {
      return {
        value: x.username,
        label: `(${x.username}) ${x.name}`,
        detail: {
          username: x.username,
          full_name: x.name,
        },
      };
    });
    return result;
  } catch (err) {
    debug(err);
    return err;
  }
};

const getDownlineSelectMobile = async (query) => {
  try {
    let get_auth = await Authentication.findOne({
      is_active: true,
      username: query.username ? query.username : query.requester,
    });

    if (helper.isEmptyObject(get_auth)) {
      return [];
    }

    let get_segment = await Segment.findOne({
      segment_code: query.segment_code,
    });

    let next = true;
    let array_username = [
      {
        username: get_auth.username,
        role_code: query.role_code,
        role_name: query.role_name,
        value: get_auth.username,
        segment_code: query.segment_code,
        segment_name: helper.isEmptyObject(get_segment)
          ? ""
          : get_segment.segment_name,
        label: `(${get_auth.username}) ${get_auth.name} - ${query.role_name}`,
        detail: {
          username: get_auth.username,
          full_name: get_auth.name,
        },
      },
    ];
    let username_search = [];
    username_search.push(query.requester);
    while (next && username_search.length !== 0) {
      let usnm = username_search.shift();
      let downline = await Authentication.find({
        is_active: true,
        role: {
          $elemMatch: {
            parent_code: { $in: [usnm] },
            segment_code: query.segment_code,
          },
        },
      }).select("username name role -_id");

      downline.map((x) => {
        username_search.push(x.username);
        x.role.map((y) => {
          if (y.parent_code == usnm) {
            array_username.push({
              username: x.username,
              role_code: y.role_code,
              role_name: y.role_name,
              segment_code: y.segment_code,
              segment_name: y.segment_name,
              value: x.username,
              label: `(${x.username}) ${x.name} - ${y.role_name}`,
              detail: {
                username: x.username,
                full_name: x.name,
              },
            });
          }
        });
      });
    }
    debug(array_username, " => result get downline mobile");
    return array_username;
  } catch (err) {
    debug(err);
    return err;
  }
};

const searchDownline = async (segment_code, requester) => {
  try {
    let next = true;
    let array_username = [];
    let username_search = [];
    username_search.push(requester);
    while (next && username_search.length !== 0) {
      let usnm = username_search.shift();
      let downline = await Authentication.find({
        is_active: true,
        role: {
          $elemMatch: {
            parent_code: { $in: [usnm] },
            segment_code: segment_code,
          },
        },
      }).select("username -_id");

      downline.map((x) => {
        username_search.push(x.username);
      });
      array_username.push(usnm);
    }
    return array_username;
  } catch (err) {
    debug(err);
    return err;
  }
};

const getUpperParent = async (query) => {
  try {
    const usr = await Authentication.findOne({
      is_active: true,
      username: query.requester,
    });

    if (helper.isEmptyObject(usr)) {
      return [];
    }

    debug(usr, "=> usr");

    let find_role;
    for (x of usr.role) {
      if (
        x.role_code == query.role_code &&
        x.segment_code == query.segment_code
      ) {
        find_role = x;
        break;
      }
    }
    debug(find_role, "=> find role");

    if (!find_role) {
      return [];
    }

    let nomor = 0;
    let username_search = [];
    username_search.push(find_role.parent_code);
    let role_code = query.role_code;
    let next = true;
    let approval_list = [];
    while (nomor <= 10 && next && username_search.length !== 0) {
      let usnm = username_search.shift();
      let upper = await Authentication.findOne({
        username: usnm,
      });

      if (!upper) {
        continue;
      }

      const get_role = await Roles.findOne({ role_code: role_code });
      if (helper.isEmptyObject(get_role)) {
        next = false;
      }
      role_code = get_role.parent_role_code;

      debug(upper + " ====> role");
      let find_pre = upper.role.find(
        (x) => x.role_code == role_code && x.segment_code == query.segment_code
      );

      if (typeof find_pre !== "undefined") {
        username_search.push(find_pre.parent_code);
      }

      approval_list.push({
        username: upper.username,
        name: upper.name,
        email: upper.email,
        value: upper.username,
        label: `(${upper.username}) ${upper.name}`,
      });
      nomor = nomor + 1;
    }

    return approval_list;
  } catch (err) {
    debug(err);
    return [];
  }
};

const getUserRoleByParent = async (query) => {
  try {
    let get_user = await Authentication.findOne({
      is_active: true,
      username: query.username,
    });

    if (helper.isEmptyObject(get_user)) {
      return [];
    }

    let result = [];
    get_user.role.map((x) => {
      if (x.segment_code == query.segment_code) {
        result.push({
          value: x.role_code,
          label: `(${x.role_code}) ${x.role_name}`,
          detail: {
            role_code: x.role_code,
            role_name: x.role_name,
          },
        });
      }
    });

    return result;
  } catch (err) {
    debug(err);
    return err;
  }
};

const create = async (payload) => {
  try {
    debug(payload, "===> PARAMS CREATE USER");

    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const findFilter = {
      is_active: true,
      $or: [{ username: payload.username }, { email: payload.email }],
    };

    const old_data = await Authentication.findOne(findFilter);

    // check old data exists
    if (!helper.isEmptyObject(old_data)) {
      let message = "Error";
      if (old_data.username == payload.username) {
        message =
          "user already exists. try changing username or updating existing data";
      } else if (old_data.email == payload.email) {
        message =
          "email already exists. try changing email or updating existing data";
      }
      return error.errorReturn({ message });
    }

    const new_value = {};
    new_value.department = payload.department;
    new_value.username = payload.username;
    new_value.password = await Encryptor.hash(payload.username);
    new_value.name = payload.name;
    new_value.email = payload.email;
    new_value.type = payload.type;
    new_value.detail_user = payload.detail_user;
    new_value.role = payload.role;
    new_value.is_active = true;
    new_value.created_at = new Date();
    new_value.created_by = payload.requester;

    const result = await new Authentication(new_value).save();

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};
const read = async (query) => {
  try {
    debug(query, "===> PARAMS READ USER");

    if (helper.isEmptyObject(query.segment_code)) {
      return error.errorReturn({ message: "segment_code is required" });
    }

    //QUERY FILTER
    let condition = {
      is_active: true,
      role: {
        $elemMatch: { segment_code: query.segment_code },
      },
    };

    // SORT by ASC or DESC
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "created_at";
    const sort = query.sortBy ? { [sortField]: sortOrder } : { created_at: -1 };

    //SKIP
    const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;

    //LIMIT
    const limit = sizePerPage;

    let findData = [];

    if (query.show_all) {
      findData = await Authentication.find(condition)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .select("-__v");
    } else {
      findData = await Authentication.find(condition)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .skip(skip)
        .limit(limit)
        .select("-__v");
    }

    if (helper.isEmptyObject(findData)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }

    const numFound = await Authentication.find(condition);
    const result = {
      foundData: findData,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const readSelect = async (query) => {
  try {
    debug(query, "===> PARAMS READ SELECT USER");

    if (helper.isEmptyObject(query.segment_code)) {
      return error.errorReturn({ message: "segment is required" });
    }

    if (helper.isEmptyObject(query.role_code)) {
      return error.errorReturn({ message: "role is required" });
    }

    // define query get parent of role
    const filter_role = { is_active: true, role_code: query.role_code };

    // get parent of role
    const result_parent_role = await Roles.findOne(filter_role);

    // define query get data user by role
    const filter_user = {
      is_active: true,
      role: {
        $elemMatch: {
          role_code: result_parent_role.parent_role_code,
        },
      },
    };

    // SORT by ASC or DESC
    const sort = { username: "asc" };

    // find data user by filter parent of role
    const authentication = await Authentication.find(filter_user)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .select("-__v");

    // restructure data select to result
    const result = authentication.map((item) => {
      return {
        value: item.username,
        label: `(${item.username}) ${item.name}`,
        detail: {
          parent_code: item.username,
          parent_name: item.name,
        },
      };
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const update = async (payload) => {
  try {
    debug(payload, "===> PARAMS UPDATE USER");

    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const id = payload._id;

    delete payload._id;
    const old_data = await Authentication.findOne({ _id: id }).lean();

    const new_value = {};
    new_value.detail_user = payload.detail_user;
    new_value.role = handleUpdateRole(
      old_data.role,
      payload.role,
      payload.segment_code
    );
    if (payload.department === "guest") {
      new_value.name = payload.name;
      new_value.email = payload.email;
    }
    new_value.updated_at = new Date();
    new_value.updated_by = payload.requester;

    const result = await Authentication.findOneAndUpdate(
      { _id: id },
      new_value,
      {
        useFindAndModify: false,
        new: true,
      }
    );

    delete old_data._id;
    old_data.id_history = result._id;

    const result_history = await new AuthenticationHistory(old_data).save();

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const destroy = async (payload) => {
  try {
    debug(payload, "===> PARAMS DELETE USER");

    const validate = validateDeleteSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    let id = payload._id;
    let filter = { _id: id };

    let result = await Authentication.findOneAndUpdate(filter, {
      is_active: false,
      deleted_at: new Date(),
      deleted_by: payload.requester,
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const findUser = async (payload) => {
  try {
    debug(payload, "===> PARAMS FIND USER");

    if (helper.isEmptyObject(payload.username)) {
      return error.errorReturn({ message: "username is required" });
    }

    if (helper.isEmptyObject(payload.segment_code)) {
      return error.errorReturn({ message: "segment_code is required" });
    }

    const filter = {
      is_active: true,
      username: payload.username,
    };

    const find_user = await Authentication.find(filter);

    return { message: "User is available", is_available: true };
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

// handle custom role
const handleUpdateRole = (old_role, new_role, segment_code) => {
  const new_value = [...old_role];

  // hapus data segment yang sama
  const new_filter_role = new_value.filter(
    (item) => item.segment_code !== segment_code
  );

  // tambah dan perbarui dengan data segment yang baru
  Array.prototype.push.apply(new_filter_role, new_role);

  debug(new_filter_role, "===> RESULT");

  return new_filter_role;
};

const updateLastLogin = async (payload) => {
  try {
    let user = await Authentication.findOne({
      is_active: true,
      username: payload.username,
    });
    if (helper.isEmptyObject(user)) {
      return error.errorReturn({ message: "user is non active" });
    }

    const result = await Authentication.findOneAndUpdate(
      { _id: user._id },
      { last_login: new Date(), app_version: payload.app_version },
      {
        useFindAndModify: false,
        new: true,
      }
    );
    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const updateFcmToken = async (payload) => {
  try {
    debug(payload, " => payload updateFcmToken");
    let user = await Authentication.findOne({
      username: payload.username,
    });
    if (helper.isEmptyObject(user)) {
      return error.errorReturn({ message: "user not found" });
    }

    const result = await Authentication.findOneAndUpdate(
      { _id: user._id },
      { fcm_token: payload.token, os: payload.os },
      {
        useFindAndModify: false,
        new: true,
      }
    );
    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const getSingleUser = async (payload) => {
  try {
    let user = await Authentication.findOne({
      is_active: true,
      username: payload.username,
    });
    if (helper.isEmptyObject(user)) {
      return {};
    }

    return user;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const updateProfile = async (payload) => {
  try {
    debug(payload, " => payload");
    const result = await Authentication.findOneAndUpdate(
      { _id: payload._id },
      { email: payload.email, address: payload.address, phone: payload.phone },
      {
        useFindAndModify: false,
        new: true,
      }
    );

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};

const updatePassword = async (payload) => {
  try {
    let user = await Authentication.findOne({
      is_active: true,
      username: payload.requester,
      _id: payload._id,
    });
    if (helper.isEmptyObject(user)) {
      return {};
    }

    if (user.type == "guest") {
      if (!(await Encryptor.verify(payload.old_password, user.password))) {
        return error.errorReturn({ message: "old password is wrong!" });
      }

      if (payload.new_password != payload.new_password_2) {
        return error.errorReturn({ message: "password is not match" });
      }

      let new_password = await Encryptor.hash(payload.new_password);
      const result = await Authentication.findOneAndUpdate(
        { _id: payload._id },
        { password: new_password },
        {
          useFindAndModify: false,
          new: true,
        }
      );

      return result;
    }

    if (user.type == "employee") {
      let parameters = {
        method: "PUT",
        url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/user/password/change/`,
        headers: {
          "x-application-token": `${payload.app_token}`,
          "x-public-key": `${process.env.PUBLIC_KEY}`,
          "x-user-token": `${payload.user_token}`,
        },
      };
      const new_data = {};
      new_data.old_password = payload.old_password;
      new_data.new_password = payload.new_password;
      parameters.data = new_data;

      debug(parameters, "paramater change password");

      const res_change_password = await axios(parameters).then(
        (response) => {
          debug(response.data);
          return response;
        },
        (error) => {
          return error.response.data;
        }
      );
      debug(res_change_password);
      if (res_change_password.error) {
        return error.errorReturn({
          message: res_change_password.message,
        });
      }
      return res_change_password;
    }

    return {};
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
};
module.exports = {
  getSendNotifUser,
  getAllDepartment,
  getUserByDept,
  getUserByDeptwithFilter,
  getDownline,
  getDownlineSelect,
  getDownlineSelectMobile,
  getUpperParent,
  getUserRoleByParent,
  create,
  read,
  readSelect,
  update,
  destroy,
  findUser,
  updateLastLogin,
  getSingleUser,
  updateProfile,
  updatePassword,
  updateFcmToken,
};
