const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:authorization-module");
const helper = require("../common/helper");
const db = require("../models");
const Authorization = db.authorization;

const createSchema = Joi.object({
  role: Joi.string().required(),
  menu: Joi.array().required(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  role: Joi.string().required(),
  menu: Joi.array().required(),
});

const roleSchema = Joi.object({
  role: Joi.string().required(),
});
const roleAndSegmentSchema = Joi.object({
  role_code: Joi.string().required(),
  segment_code: Joi.string().required(),
});
function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}
function validateRoleSchema(schema) {
  return roleSchema.validate(schema);
}

function validateRoleAndSegmentSchema(schema) {
  return roleAndSegmentSchema.validate(schema);
}

async function createAuthorization(payload_params) {
  try {
    let payload = payload_params;

    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    payload.isDeleted = false;
    const authorization = await new Authorization(payload).save();
    return authorization;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

function updateAuthorization(payload) {
  try {
    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      debug(validate.error);
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    const filter = { _id: payload._id };
    let update = payload;
    let authorization = Authorization.findOneAndUpdate(filter, update, {
      useFindAndModify: false,
      new: true,
    });
    if (helper.isEmptyObject(authorization)) {
      return error.errorReturn({ message: "Authorization not found" });
    }

    return authorization;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}
function deleteAuthorization(payload) {
  try {
    const validate = validateIdSchema(payload);
    if (validate.error) {
      debug(validate.error);
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    const filter = { _id: payload._id };
    const update = {
      updated_at: new Date(`UTC+7`),
      isDeleted: true,
    };
    let authorization = Authorization.findOneAndUpdate(filter, update, {
      useFindAndModify: false,
      new: true,
    });
    if (helper.isEmptyObject(authorization)) {
      return error.errorReturn({ message: "Authorization not found" });
    }

    return authorization;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}
async function readAllAuthorization() {
  try {
    const filter = {
      isDeleted: false,
    };
    let authorization = await Authorization.findOne(filter).select(
      "-__v  -isDeleted"
    );

    if (helper.isEmptyObject(result)) {
      return error.errorReturn({ message: "Authorization not found" });
    }

    return authorization;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function readByRoleAuthorization(payload) {
  try {
    const validate = validateRoleSchema(payload);
    if (validate.error) {
      debug(validate.error);
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    let filter = { role: payload.role };

    let authorization = await Authorization.findOne(filter).select(
      "-__v -isDeleted -_id"
    );
    debug(authorization);

    if (helper.isEmptyObject(authorization)) {
      filter = { role: "guest user" };

      authorization = await Authorization.findOne(filter).select(
        "-__v -isDeleted -_id"
      );

      return authorization.menu;
    }

    return authorization.menu;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function readByRoleAndSegment(payload) {
  try {
    const validate = validateRoleAndSegmentSchema(payload);
    if (validate.error) {
      debug(validate.error);
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    let filter = {
      role_code: payload.role_code,
      segment_code: payload.segment_code,
    };

    let authorization = await Authorization.findOne(filter).select(
      "-__v -isDeleted -_id"
    );
    debug(authorization);

    if (helper.isEmptyObject(authorization)) {
      filter = { role: "guest user" };

      authorization = await Authorization.findOne(filter).select(
        "-__v -isDeleted -_id"
      );

      return authorization;
    }

    return authorization;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  createAuthorization,
  updateAuthorization,
  deleteAuthorization,
  readByRoleAuthorization,
  readByRoleAndSegment,
  readAllAuthorization,
};
