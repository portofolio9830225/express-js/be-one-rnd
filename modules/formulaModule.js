const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:formula-module");
const mongoose = require("mongoose");
const helper = require("../common/helper");
const db = require("../models");
const moment = require("moment");
const Ticket_F = db.ticket_f;
const Formula_F = db.formula_f;
const Status = db.status;
const FormData = require("form-data");
const path = require("path");
const { readFileSync, unlinkSync } = require("fs");
const axios = require("axios");
const zeroPad = require("../helpers/zeroPad");

const fileModule = require("./fileModule");

const formulasiSchema = Joi.object({
	_id: Joi.string().required(),
	requester: Joi.string().required(),
	formula: Joi.array().required(),
	product_properties: Joi.array().allow(null),
	fg_name: Joi.string(),
	fg_code: Joi.string(),
	person_name: Joi.string(),
});

const sensoryFormulaSchema = Joi.object({
	_id: Joi.string().required(),
	requester: Joi.string().required(),
	files: Joi.array().allow(null).allow(""),
	sensory: Joi.array().allow(null).allow(""),
	app_token: Joi.string().allow(null).allow(""),
	remark: Joi.string().allow(null).allow(""),
});

const updateStatusFormulaSchema = Joi.object({
	_id: Joi.string().required(),
	requester: Joi.string().required(),
	update: Joi.array().required(),
	ticket_id: Joi.string(),
	remark: Joi.string(),
});

function validateFormulasi(schema) {
	return formulasiSchema.validate(schema);
}
function validateSensoryFormulaSchema(schema) {
	return sensoryFormulaSchema.validate(schema);
}
function validateUpdateStatusFormulaSchema(schema) {
	return updateStatusFormulaSchema.validate(schema);
}

async function read(query) {
	try {
		debug("read");

		let condition = {};
		condition.is_active = true;
		condition.status_code = {
			$in: ["sedang_formulasi", "siap_formulasi"],
		};
		if (query.target_date_start && query.target_date_finish) {
			condition.target_date = {
				$gte: moment(query.target_date_start).startOf("day"),
				$lte: moment(query.target_date_finish).startOf("day"),
			};
		}

		debug(condition);

		//sort
		const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query.sortBy ? query.sortBy : "date";
		const sort = query.sortBy
			? {
					[sortField]: sortOrder,
			  }
			: {
					date: 1,
			  };

		//skip
		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;
		//limit
		const limit = sizePerPage;

		const ck = await Ticket_F.find(condition)
			.sort(sort)
			.collation({ locale: "en_US", numericOrdering: true })
			.skip(skip)
			.limit(limit)
			.select("-__v");

		if (helper.isEmptyObject(ck)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}
		const numFound = await Ticket_F.find(condition);
		const result = {
			foundData: ck,
			currentPage: page,
			countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
			countData: Object.keys(numFound).length,
		};

		return result;
	} catch (err) {
		debug(err);
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function formulasi(payload) {
	try {
		debug(payload);
		const validate = validateFormulasi(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const getTicket = await Ticket_F.findOne({
			is_active: true,
			_id: payload._id,
		}).lean();
		if (helper.isEmptyObject(getTicket))
			return error.errorReturn({
				message: "Document is not found, please reload your page",
			});

		const firstStatus = await Status.findOne({
			is_active: true,
			status_code: "sedang_formulasi",
		}).lean();

		// payload.formula.forEach(async (elm) => {
		let i = 0;
		let isUpdate = false;
		for (elm of payload.formula) {
			if (elm.editable != false) {
				//generate queue number
				let new_date = new Date();
				let getOldData = await Formula_F.find({
					created_at: {
						$gte: moment(new_date).startOf("date"),
					},
					queue_number: { $exists: true },
				}).exec();

				urutan = getOldData.length + 1;
				debug(urutan, " urutan");

				//sodiq 09:57 15-01-2023, for product properties
				let _product_properties = [];
				if (payload.product_properties[i]?.properties !== undefined) {
					if (payload.product_properties[i]?.properties.length > 0) {
						for (let obj of payload.product_properties[i]?.properties) {
							if (obj.product_properties.value !== "") {
								_product_properties.push({
									properties: obj.product_properties.value,
									value: obj.value,
								});
							}
						}
					}
				}

				let formula = {
					job_number: null,
					queue_number: elm.queue_number
						? elm.queue_number
						: moment(new Date()).format("YYYY-MM-DD") + "-" + zeroPad.zeroPad(urutan, 3),
					sample_product: getTicket.sample_product,
					customer_code: getTicket.customer_code,
					customer_name: getTicket.customer_name,
					document_number: getTicket.document_number,
					target_date: new Date(getTicket.target_date),
					editable: true,
					status: firstStatus.status_name,
					status_code: firstStatus.status_code,
					result_fg_name: payload.fg_name,
					result_fg_code: payload.fg_code,
					created_by: payload.requester,
					created_at: new_date,
					is_active: true,
					history: [],
					due_date_miniplant: new Date(elm.due_date_miniplant),
					formula: elm.formula,
					details: elm.details,
					pic: payload.requester,
					pic_name: payload.person_name,
					pic_ticket: getTicket.pic,
					pic_ticket_name: getTicket.pic_name,
					ticket_id: mongoose.Types.ObjectId(getTicket._id),
					product_properties: _product_properties,
				};
				let history = [
					{
						requester: payload.requester,
						data: null,
						status: firstStatus.status_name,
						status_code: firstStatus.status_code,
						remark: "",
						date: new Date(),
					},
				];
				if (elm._id) {
					const getFormula = await Formula_F.findOne({
						is_active: true,
						_id: mongoose.Types.ObjectId(elm._id),
					});
					if (!helper.isEmptyObject(getFormula)) {
						//update here
						formula.history = [...getFormula.history, ...history];
						let _update = await Formula_F.findOneAndUpdate(
							{ _id: mongoose.Types.ObjectId(elm._id) },
							formula,
							{
								useFindAndModify: false,
								new: true,
							}
						);
					}
					isUpdate = true;
				} else {
					//insert here
					formula.history = history;
					let _insert = await new Formula_F(formula).save();
				}
				debug("end of loop");
			}
			i++;
		}
		// });

		return { status: isUpdate ? "Data has been updated successfully" : "Data has been created successfully" };
	} catch (err) {
		debug(err);
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function readAllFormula(query) {
	try {
		const getTicket = await Ticket_F.findOne({
			is_active: true,
			_id: query.ticket_id,
		}).lean();
		if (helper.isEmptyObject(getTicket))
			return error.errorReturn({
				message: "Document is not found, please reload your page",
			});

		const getFormula = await Formula_F.find({
			is_active: true,
			ticket_id: mongoose.Types.ObjectId(query.ticket_id),
		}).lean();

		let detailFOrmula = getFormula.map((x, index) => {
			return {
				...x,
				title: x.job_number ? x.job_number : "New Formula",
				due_date_miniplant: moment(x.due_date_miniplant).format("YYYY-MM-DD"),
				fg_code: x.result_fg_code,
				fg_name: x.result_fg_name,
				index: index,
			};
		});

		return detailFOrmula;
	} catch (err) {
		debug(err);
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function readAllFormulaWithOutParam() {
	try {
		const ck = await Formula_F.find({ is_active: true })
			.collation({ locale: "en_US", numericOrdering: true })
			.select("-__v");

		let result = ck.map(x => {
			let jn_number = x.job_number ? `${x.job_number} - ` : "";
			let fg_name = x.result_fg_name ? `(${x.result_fg_name})` : "";
			let label = `${jn_number} ${x.customer_name}, ${x.document_number} ${fg_name}`;
			if (!jn_number && !x.document_number) {
				label = `Master Formula ${fg_name}`;
			}
			return {
				value: x._id,
				label: label,
				detail: x,
			};
		});

		return result;
	} catch (err) {
		debug(err);
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function nextTrialProcess(payload) {
	try {
		const getTicket = await Ticket_F.findOne({
			is_active: true,
			_id: payload._id,
		}).lean();
		if (helper.isEmptyObject(getTicket))
			return error.errorReturn({
				message: "Document is not found, please reload your page",
			});

		const getFormula = await Formula_F.find({
			is_active: true,
			ticket_id: mongoose.Types.ObjectId(payload._id),
			editable: true,
		}).lean();

		if (helper.isEmptyArray(getFormula))
			return error.errorReturn({
				message: "Formula not found, please input formula first",
			});

		const firstFormula = await Status.findOne({
			is_active: true,
			status_code: "siap_penjadwalan",
		}).lean();

		let history_formula = [
			{
				requester: payload.requester,
				data: null,
				status: firstFormula.status_name,
				status_code: firstFormula.status_code,
				remark: "",
				date: new Date(),
			},
		];
		getFormula.forEach(async elm => {
			if (elm.editable) {
				let formula = {
					...elm,
					history: [...elm.history, ...history_formula],
					status: firstFormula.status_name,
					status_code: firstFormula.status_code,
				};

				//update here
				await Formula_F.findOneAndUpdate({ _id: mongoose.Types.ObjectId(elm._id) }, formula, {
					useFindAndModify: false,
					new: true,
				});
			}
		});

		let update = {};
		update.history = [...getTicket.history, ...history_formula];
		update.status_code = firstFormula.status_code;
		update.status = firstFormula.status_name;
		await Ticket_F.findOneAndUpdate({ _id: mongoose.Types.ObjectId(payload._id) }, update, {
			useFindAndModify: false,
			new: true,
		});

		return {};
	} catch (err) {
		debug(err);
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function deleteFormula(payload) {
	try {
		debug(payload, " => payload");
		const getFormula = await Formula_F.find({
			is_active: true,
			job_number: null,
			editable: true,
			_id: mongoose.Types.ObjectId(payload._id),
		}).lean();

		if (helper.isEmptyObject(getFormula)) {
			return error.errorReturn({
				message: "Document can't delete or not found",
			});
		}

		let update = {};
		update.is_active = false;
		update.updated_at = new Date();
		update.updated_by = payload.requester;
		await Formula_F.findOneAndUpdate({ _id: mongoose.Types.ObjectId(payload._id) }, update, {
			useFindAndModify: false,
			new: true,
		});

		return {};
	} catch (err) {
		debug(err);
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function readFormula(query) {
	try {
		debug(query);
		debug("==> PAYLOAD READ FORMULA");

		// query
		let filter = { is_active: true };

		if (query?.status) {
			filter = { ...filter, status: { $in: query.status } };
		}

		if (query?.date_start && query?.date_end) {
			filter = {
				...filter,
				created_at: { $gte: query.date_start, $lte: query.date_end },
			};
		}

		// sort
		const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query?.sortField ?? "created_at";
		const sort = { [sortField]: sortOrder };

		// skip
		const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
		const page = query?.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		// limit
		const limit = sizePerPage;

		// find data
		let found_data = [];

		if (query?.showAll) {
			found_data = await Formula_F.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.select("-__v")
				.lean();
		} else {
			found_data = await Formula_F.find(filter)
				.sort(sort)
				.collation({ locale: "en_US", numericOrdering: true })
				.skip(skip)
				.limit(limit)
				.select("-__v")
				.lean();
		}

		// if empty data
		if (helper.isEmptyArray(found_data)) {
			const result = {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
			debug(result);
			debug("===> RESULT READ FORMULA");
			return result;
		}

		//
		const count_data = await Formula_F.countDocuments(filter);
		const current_page = query.showAll ? 1 : page;
		const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
		const result = {
			foundData: found_data,
			currentPage: current_page,
			countPages: count_page,
			countData: count_data,
		};

		debug(result);
		debug("===> RESULT READ FORMULA");

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR READ FORMULA");
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function verificationFormula(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD VERIFICATION FORMULA");

		const find_exists = await Formula_F.findOne({ _id: payload._id }).lean();
		if (helper.isEmptyObject(find_exists)) {
			return error.errorReturn({ message: "Data not found" });
		}

		const new_files = {
			files: payload.files,
			app_token: payload.app_token,
			department: payload.department,
		};

		const result_upload = await fileModule.create(new_files);

		// return error
		if (result_upload?.error) {
			return result_upload;
		}

		const new_data = [];

		for (let file of result_upload) {
			new_data.push({ document: file.onedrive_id });
		}

		const history = [].concat(
			[...find_exists.history],
			{
				requester: payload.requester,
				data: null,
				status: "Siap Verifikasi Hasil Miniplant",
				status_code: "siap_verifikasi_hasil_miniplant",
				remark: "",
				date: new Date(),
			},
			{
				requester: payload.requester,
				data: new_data,
				status: "Sedang Verifikasi Hasil Miniplant",
				status_code: "sedang_verifikasi_hasil_miniplant",
				remark: "",
				date: new Date(),
			}
		);

		const result = await Formula_F.findOneAndUpdate(
			{ _id: mongoose.Types.ObjectId(find_exists._id) },
			{
				history,
				status: "Sedang Verifikasi Hasil Miniplant",
				status_code: "sedang_verifikasi_hasil_miniplant",
				updated_at: new Date(),
				updated_by: payload.requester,
			},
			{
				useFindAndModify: false,
				new: true,
			}
		);

		debug(result);
		debug("===> RESULT VERIFICATION FORMULA");

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR VERIFICATION FORMULA");
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function sensoryFormula(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD SENSORY FORMULA");
    payload.sensory = JSON.parse(payload.sensory);
		const validate = validateSensoryFormulaSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const find_exists = await Formula_F.findOne({ _id: payload._id }).lean();
		if (helper.isEmptyObject(find_exists)) {
			return error.errorReturn({ message: "Data not found" });
		}

		let parameters = {
			method: "POST",
			url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
			headers: {
				"x-application-token": `${payload.app_token}`,
				"x-public-key": `${process.env.PUBLIC_KEY}`,
			},
		};

		let OneDriveID = null;
		let detail = [];
		if (payload.files) {
			const form = new FormData();
			form.append("apps", process.env.NAME);

			//upload files to onedrive, get the link
			payload.files.forEach(f => {
				const readDir = path.join(__dirname, `../${f.path}`);
				const readFile = readFileSync(readDir);
				form.append("upload_file", readFile, f.path);
			});
			parameters.data = form;
			parameters.maxContentLength = Infinity;
			parameters.maxBodyLength = Infinity;
			parameters.headers["Content-Type"] = "multipart/form-data;boundary=" + form.getBoundary();

			const result = await axios(parameters)
				.then(function async(response) {
					return response.data;
				})
				.catch(function (error) {
					debug(error, "=> error callApi");
					return [];
				});
			debug(result, " result upload file");
			OneDriveID = result.map((x, i) => {
				debug(x);
				detail.push({
					mimetype: x.mimetype,
					originalname: x.originalname,
					onedriveid: x.onedrive_id,
				});
				return { document: x.onedrive_id };
			});
		}

		const new_history = {
			requester: payload.requester,
			data: OneDriveID,
			detail: detail,
			status: "Sedang Verifikasi PIC",
			status_code: "sedang_verifikasi_pic",
			remark: payload?.remark ?? "",
			date: new Date(),
		};

		const result = await Formula_F.findOneAndUpdate(
			{ _id: mongoose.Types.ObjectId(find_exists._id) },
			{
				history: [...find_exists.history, new_history],
				sensory: payload.sensory,
				status: "Sedang Verifikasi PIC",
				status_code: "sedang_verifikasi_pic",
				updated_at: new Date(),
				updated_by: payload.requester,
			},
			{
				useFindAndModify: false,
				new: true,
			}
		);

		// debug(result);
		debug(new_history);
		debug("===> RESULT SENSORY FORMULA");

		return { status: "success" };
		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR SENSORY FORMULA");
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function sensoryPlantFormula(payload) {
	try {
		const validate = validateSensoryFormulaSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const find_exists = await Formula_F.findOne({ _id: payload._id }).lean();
		if (helper.isEmptyObject(find_exists)) {
			return error.errorReturn({ message: "Data not found" });
		}

		const firstStatus = await Status.findOne({
			is_active: true,
			status_code: "sedang_validasi_hasil_trial_plant",
		}).lean();

		const new_history = {
			requester: payload.requester,
			data: null,
			status: firstStatus.status_name,
			status_code: firstStatus.status_code,
			remark: payload?.remark ?? "",
			date: new Date(),
		};

		const result = await Formula_F.findOneAndUpdate(
			{ _id: mongoose.Types.ObjectId(find_exists._id) },
			{
				history: [...find_exists.history, new_history],
				sensory_plant: payload.sensory,
				status: firstStatus.status_name,
				status_code: firstStatus.status_code,
				updated_at: new Date(),
				updated_by: payload.requester,
			},
			{
				useFindAndModify: false,
				new: true,
			}
		);

		return result;
	} catch (e) {
		debug(e);
		debug("===> ERROR SENSORY FORMULA");
		throw error.errorReturn({ message: "Internal server error" });
	}
}

async function updateStatusFormula(payload) {
	try {
		debug(payload);
		debug("===> PAYLOAD UPDATE STATUS FORMULA");

		const validate = validateUpdateStatusFormulaSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const find_exists = await Formula_F.findOne({ _id: payload._id }).lean();
		if (helper.isEmptyObject(find_exists)) {
			return error.errorReturn({ message: "Data not found" });
		}

		if (helper.isEmptyArray(payload.update)) {
			return error.errorReturn({ message: "update is empty" });
		}

		let find_ticket = null;

		// check ticket
		if (payload?.ticket_id) {
			find_ticket = await Ticket_F.findOne({
				_id: payload.ticket_id,
			}).lean();

			if (find_ticket?.error) {
				debug(find_ticket);
				debug("===> ERROR FIND TICKET");
				return error.errorReturn({ message: "Internal Server Error" });
			}

			if (helper.isEmptyObject(find_ticket)) {
				return error.errorReturn({ message: "ticket not found" });
			}
		}

		let new_status = "";
		let new_status_code = "";

		let new_history = payload.update.map(item => {
			new_status = item?.status;
			new_status_code = item?.status.replace(/[^a-zA-Z0-9]/g, "_").toLowerCase();
			return {
				requester: payload.requester,
				data: item.data,
				status: new_status,
				status_code: new_status_code,
				remark: item?.remark ?? "",
				date: new Date(),
			};
		});

		const result = await Formula_F.findOneAndUpdate(
			{ _id: mongoose.Types.ObjectId(find_exists._id) },
			{
				history: [...find_exists.history, ...new_history],
				status: new_status,
				status_code: new_status_code,
        last_remark: payload.update[0].remark,
				updated_at: new Date(),
				updated_by: payload.requester,
			},
			{
				useFindAndModify: false,
				new: true,
        strict: false,
			}
		);

		// START UPDATE TICKET
		const find_sedang_formula = new_history.findIndex(find => find.status === "Reformula");

		if (find_sedang_formula !== -1 && !result?.error && !find_ticket?.error) {
			let new_history_ticket = new_history.map(item => {
				return {
					...item,
					status: "Sedang Formulasi",
					status_code: "sedang_formulasi",
					date: new Date(),
				};
			});

			const result_update_ticket = await Ticket_F.findOneAndUpdate(
				{
					_id: mongoose.Types.ObjectId(payload.ticket_id),
				},
				{
					history: [...find_ticket.history, ...new_history_ticket],
					status: "Sedang Formulasi",
					status_code: "sedang_formulasi",
					updated_at: new Date(),
					updated_by: payload.requester,
				},
				{
					useFindAndModify: false,
					new: true,
				}
			);

			debug(result_update_ticket);
			debug("===> RESULT UPDATE TICKET");
		}
		// END UPDATE TICKET

     // COPY DATA REFORMULA
     if (payload.update[0].status === 'Reformula') {
      const copyFormula = await Formula_F.findOne({_id: payload._id});
      let formula = {
        prev_formula: payload._id,
        job_number: null,
        queue_number: null,
        sample_product: copyFormula.sample_product,
        customer_code: copyFormula.customer_code,
        customer_name: copyFormula.customer_name,
        document_number: copyFormula.document_number,
        target_date: copyFormula.target_date,
        editable: true,
        status: copyFormula.status,
        status_code: copyFormula.status_code,
        result_fg_name: copyFormula.result_fg_name,
        result_fg_code: copyFormula.result_fg_code,
        created_by: copyFormula.created_by,
        created_at: copyFormula.created_at,
        is_active: copyFormula.is_active,
        history: copyFormula.history,
        due_date_miniplant: copyFormula.due_date_miniplant,
        formula: copyFormula.formula,
        details: copyFormula.details,
        pic: copyFormula.pic,
        pic_name: copyFormula.pic_name,
        pic_ticket: copyFormula.pic_ticket,
        pic_ticket_name: copyFormula.pic_ticket_name,
        ticket_id: copyFormula.ticket_id,
        product_properties: copyFormula.product_properties,
        sensory: copyFormula.sensory,
        sensory_plant: copyFormula.sensory_plant,
        updated_at: copyFormula.updated_at,
        updated_by: copyFormula.updated_by,
        last_remark: copyFormula.last_remark
      };
      await new Formula_F(formula).save();
    }
  // END COPY

    if (payload?.ticket_id) {
      findFormula = await Formula_F.find({ticket_id: payload.ticket_id, is_active: true}).lean();
      let isDone = [];

      findFormula.map(data => {
        if (data.status === 'Selesai' || data.status === 'Drop' || data.status === 'Reformula') {
          isDone.push(true);
        }
      });

      if (isDone.length === findFormula.length) {
        const newTicketHistory = {
          requester: payload.requester,
          date: new Date(),
          data: [payload.update[0].data],
          status: 'Selesai',
          status_code: 'selesai',
          remark: payload.update[0].remark
        }

        findTicket = await Ticket_F.findOne({_id: payload.ticket_id}).lean();
        await Ticket_F.findOneAndUpdate({
        _id: payload.ticket_id,
        }, {
          status: 'Selesai',
          status_code: 'selesai',
          history: [...findTicket.history, newTicketHistory]
        }, {
          useFindAndModify: false,
          new: true,
        })
      }
    }

		debug(result);
		debug("===> RESULT UPDATE STATUS FORMULA");
		return {};
	} catch (e) {
		debug(e);
		debug("===> ERROR UPDATE STATUS FORMULA");
		throw error.errorReturn({ message: "Internal server error" });
	}
}

module.exports = {
	read,
	readFormula,
	formulasi,
	readAllFormula,
	nextTrialProcess,
	readAllFormulaWithOutParam,
	deleteFormula,
	verificationFormula,
	sensoryFormula,
	sensoryPlantFormula,
	updateStatusFormula,
};
