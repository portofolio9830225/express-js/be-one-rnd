const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:segment-module");
const mongoose = require("mongoose");
const helper = require("../common/helper");
const escapeRegex = require("../common/escapeRegex");
const db = require("../models");
const zeroPad = require("../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const Priority = db.priority;
const FormData = require("form-data");
const path = require("path");
const { readFileSync, unlinkSync } = require("fs");
const axios = require("axios");

async function exampleUpload(query) {
  try {
    if (!helper.isEmptyObject(payload.files)) {
      let parameters = {
        method: "POST",
        url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
        headers: {
          "x-application-token": `${payload.app_token}`,
          "x-public-key": `${process.env.PUBLIC_KEY}`,
        },
      };
      const form = new FormData();
      form.append("apps", process.env.NAME);

      //upload files to onedrive, get the link
      payload.files.forEach((f) => {
        const readDir = path.join(__dirname, `/../${f.path}`);
        const readFile = readFileSync(readDir);
        form.append("upload_data", readFile, f.path);
      });
      parameters.data = form;
      parameters.maxContentLength = Infinity;
      parameters.maxBodyLength = Infinity;
      parameters.headers["Content-Type"] =
        "multipart/form-data;boundary=" + form.getBoundary();

      const result = await axios(parameters);

      for (let f of payload.files) {
        // remove files when finish make call
        unlinkSync(path.join(__dirname, `/../${f.path}`));
      }

      debug(result, "=>>result");

      new_activity.menu_image = result.data[0].onedrive_id;

      if (!result.error) {
        for (let abc of result.data) {
          let init = { ...abc };
          await File.create(init);
        }
      } else {
        return error.errorReturn({ message: "Cannot upload file" });
      }
    }
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  exampleUpload,
};
