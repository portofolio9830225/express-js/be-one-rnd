const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:segment-module");
const mongoose = require("mongoose");
const helper = require("../common/helper");
const escapeRegex = require("../common/escapeRegex");
const db = require("../models");
const zeroPad = require("../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const Vendor = db.vendor;

async function readSelect(query) {
  try {
    debug("run vendor");
    let condition = {
      is_active: true,
    };

    const ck = await Vendor.find(condition).select("-__v");

    let result = ck.map((x) => {
      return {
        value: x.code,
        label: x.code ? `(${x.code})  ${x.name}` : x.name,
      };
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  readSelect,
};
