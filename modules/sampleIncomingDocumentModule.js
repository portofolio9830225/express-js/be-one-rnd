const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-one-rnd:segment-module");
const mongoose = require("mongoose");
const helper = require("../common/helper");
const escapeRegex = require("../common/escapeRegex");
const db = require("../models");
const zeroPad = require("../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const SampleIncomingDocument = db.sample_incoming_document;

async function readSelect(query) {
  try {
    let condition = {
      is_active: true,
    };

    const ck = await SampleIncomingDocument.find(condition).select("-__v");

    let result = ck.map((x) => {
      return {
        value: x.name,
        label: x.name,
      };
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  readSelect,
};
