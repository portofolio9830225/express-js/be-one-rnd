const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:schedule-module");
const mongoose = require("mongoose");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const db = require("../../models");
const zeroPad = require("../../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const Ticket_F = db.ticket_f;
const Formula_F = db.formula_f;
const Status = db.status;

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  priority: Joi.object().required(),
  schedule_date: Joi.string().required(),
  remark: Joi.string().allow(null).allow(""),
});

function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}

async function schedule(payload) {
  try {
    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    let _id = payload._id;

    let check_data = await Formula_F.findOne({
      is_active: true,
      _id: mongoose.Types.ObjectId(_id),
    }).lean();
    if (helper.isEmptyObject(check_data))
      return error.errorReturn({
        message: "Data is not found, please reload your page",
      });

    let status = await Status.findOne({
      status_code: "sedang_penjadwalan",
      is_active: true,
    });
    let status2 = await Status.findOne({
      status_code: "siap_trial_miniplant",
      is_active: true,
    });

    let all_status = [
      {
        requester: payload.requester,
        data: null,
        status: status.status_name,
        status_code: status.status_code,
        remark: payload.remark,
        date: new Date(),
      },
      {
        requester: payload.requester,
        data: null,
        status: status2.status_name,
        status_code: status2.status_code,
        remark: null,
        date: new Date(),
      },
    ];

    let priority = payload.priority;

    let new_payload = {};

    new_payload.updated_at = new Date();
    new_payload.updated_by = payload.requester;
    new_payload.schedule = new Date(payload.schedule_date);
    new_payload.priority = priority.value;
    new_payload.priority_color = priority.color;
    new_payload.status = status2.status_name;
    new_payload.status_code = status2.status_code;
    new_payload.schedule_date = new Date(payload.schedule_date);
    new_payload.history = [...check_data.history, ...all_status];
    delete new_payload._id;

    let filter = { _id: mongoose.Types.ObjectId(_id) };
    let result = await Formula_F.findOneAndUpdate(filter, new_payload, {
      useFindAndModify: false,
      new: true,
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function read(query) {
  try {
    let condition = {};
    condition.is_active = true;
    condition.status_code = {
      $in: ["open", "sedang_penjadwalan", "siap_penjadwalan"],
    };
    if (query.target_date_start && query.target_date_finish) {
      condition.target_date = {
        $gte: moment(query.target_date_start).startOf("day"),
        $lte: moment(query.target_date_finish).startOf("day"),
      };
    }

    debug(condition, " => condition");

    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "target_date";
    const sort = query.sortBy
      ? {
          [sortField]: sortOrder,
        }
      : {
          target_date: 1,
        };

    //skip
    const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;
    //limit
    const limit = sizePerPage;

    const ck = await Formula_F.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(ck)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await Formula_F.find(condition);
    const result = {
      foundData: ck,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  read,
  schedule,
};
