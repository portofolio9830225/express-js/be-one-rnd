const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:trial-module");
const mongoose = require("mongoose");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const db = require("../../models");
const zeroPad = require("../../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const Ticket_F = db.ticket_f;
const Formula_F = db.formula_f;
const Status = db.status;
const FormData = require("form-data");
const path = require("path");
const { readFileSync, unlinkSync } = require("fs");
const axios = require("axios");

const startTrialSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  files: Joi.array().allow(null).allow(""),
  app_token: Joi.string().allow(null).allow(""),
});

const finishTrialSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  files: Joi.array().allow(null).allow(""),
  app_token: Joi.string().allow(null).allow(""),
});

function validateStartTrialSchema(schema) {
  return startTrialSchema.validate(schema);
}

function validateFinishTrialSchema(schema) {
  return finishTrialSchema.validate(schema);
}

async function stratTrial(payload) {
  try {
    const validate = validateStartTrialSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    let getFormula = await Formula_F.findOne({
      _id: mongoose.Types.ObjectId(payload._id),
      is_active: true,
    });
    if (helper.isEmptyObject(getFormula))
      return error.errorReturn({
        message: "Formula is not found, please reload your page",
      });

    let getOldData = await Formula_F.find({ job_number: { $ne: null } })
      .sort({ job_number: -1 })
      .lean();
    let urutan = 0;
    if (!helper.isEmptyArray(getOldData)) {
      urutan = getOldData.length;
    }

    let parameters = {
      method: "POST",
      url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
      headers: {
        "x-application-token": `${payload.app_token}`,
        "x-public-key": `${process.env.PUBLIC_KEY}`,
      },
    };

    let OneDriveID = null;
    if (payload.files) {
      const form = new FormData();
      form.append("apps", process.env.NAME);

      //upload files to onedrive, get the link
      payload.files.forEach((f) => {
        const readDir = path.join(__dirname, `../../${f.path}`);
        const readFile = readFileSync(readDir);
        form.append("upload_file", readFile, f.path);
      });
      parameters.data = form;
      parameters.maxContentLength = Infinity;
      parameters.maxBodyLength = Infinity;
      parameters.headers["Content-Type"] =
        "multipart/form-data;boundary=" + form.getBoundary();

      const result = await axios(parameters)
        .then(function async(response) {
          return response.data;
        })
        .catch(function (error) {
          debug(error, "=> error callApi");
          return [];
        });
      debug(result, " result upload file");
      OneDriveID = result.map((x) => {
        return { document: x.onedrive_id };
      });
    }
    //get status
    let status = await Status.findOne({
      status_code: "sedang_trial_plant",
      is_active: true,
    });
    let status_formula = [
      {
        requester: payload.requester,
        data: OneDriveID,
        status: status.status_name,
        status_code: status.status_code,
        remark: "Start Trial Plant",
        date: new Date(),
      },
    ];

    let new_payload = {};
    new_payload.updated_at = new Date();
    new_payload.updated_by = payload.requester;
    new_payload.status = status.status_name;
    new_payload.status_code = status.status_code;
    new_payload.history = getFormula.history
      ? [...getFormula.history, ...status_formula]
      : status_formula;
    debug(new_payload);

    //update formula
    let filter = { _id: mongoose.Types.ObjectId(payload._id) };
    let result = await Formula_F.findOneAndUpdate(filter, new_payload, {
      useFindAndModify: false,
      new: true,
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function finishTrial(payload) {
  try {
    const validate = validateFinishTrialSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    let getFormula = await Formula_F.findOne({
      _id: mongoose.Types.ObjectId(payload._id),
      is_active: true,
    });
    if (helper.isEmptyObject(getFormula))
      return error.errorReturn({
        message: "Formula is not found, please reload your page",
      });

    //get status
    let status = await Status.findOne({
      status_code: "siap_validasi_hasil_trial_plant",
      is_active: true,
    });

    let parameters = {
      method: "POST",
      url: `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/file/onedrive/upload-file/`,
      headers: {
        "x-application-token": `${payload.app_token}`,
        "x-public-key": `${process.env.PUBLIC_KEY}`,
      },
    };

    let OneDriveID = null;
    if (payload.files) {
      const form = new FormData();
      form.append("apps", process.env.NAME);

      //upload files to onedrive, get the link
      payload.files.forEach((f) => {
        const readDir = path.join(__dirname, `../../${f.path}`);
        const readFile = readFileSync(readDir);
        form.append("upload_file", readFile, f.path);
      });
      parameters.data = form;
      parameters.maxContentLength = Infinity;
      parameters.maxBodyLength = Infinity;
      parameters.headers["Content-Type"] =
        "multipart/form-data;boundary=" + form.getBoundary();

      const result = await axios(parameters)
        .then(function async(response) {
          return response.data;
        })
        .catch(function (error) {
          debug(error, "=> error callApi");
          return [];
        });
      debug(result, " result upload file");
      OneDriveID = result.map((x) => {
        return { document: x.onedrive_id };
      });
    }

    let status_formula = [
      {
        requester: payload.requester,
        data: OneDriveID,
        status: status.status_name,
        status_code: status.status_code,
        remark: "Finish Trial Plant",
        date: new Date(),
      },
    ];

    let new_payload = {};
    new_payload.updated_at = new Date();
    new_payload.updated_by = payload.requester;
    new_payload.status = status.status_name;
    new_payload.status_code = status.status_code;
    new_payload.history = getFormula.history
      ? [...getFormula.history, ...status_formula]
      : status_formula;

    //update formula
    let filter = { _id: mongoose.Types.ObjectId(payload._id) };
    let result = await Formula_F.findOneAndUpdate(filter, new_payload, {
      useFindAndModify: false,
      new: true,
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  stratTrial,
  finishTrial,
};
