const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:schedule-module");
const mongoose = require("mongoose");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const db = require("../../models");
const zeroPad = require("../../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const AdditionalInfromation_F = db.additional_information_f;

async function readSelect(query) {
  try {
    let condition = {};
    condition.is_active = true;
    if (query.category) {
      condition.category = query.category;
    }
    if (query.category_code) {
      condition.category_code = query.category_code;
    }
    debug(condition);
    debug("===> FILTER");
    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "name";
    const sort = query.sortBy
      ? {
          [sortField]: sortOrder,
        }
      : {
          name: 1,
        };

    const ck = await AdditionalInfromation_F.find(condition)
      .sort(sort)
      .select("-__v");

    debug(ck);
    debug("===> RESULT");

    let result = ck.map((x) => {
      return {
        value: x.name,
        label: x.name,
        type: x.type,
      };
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  readSelect,
};
