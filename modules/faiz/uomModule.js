const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:currency-module");
const mongoose = require("mongoose");
const db = require("../../models");
const UOM = db.uom;

async function readSelect(query) {
  try {
    let condition = {
      is_active: true,
    };

    const ck = await UOM.find(condition).select("-__v");

    let result = ck.map((x) => {
      return {
        value: x.code,
        label: x.name,
      };
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  readSelect,
};
