const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:trial-module");
const mongoose = require("mongoose");
const helper = require("../../common/helper");
const escapeRegex = require("../../common/escapeRegex");
const db = require("../../models");
const zeroPad = require("../../helpers/zeroPad");
const dateFormat = require("dateformat");
const moment = require("moment");
const Ticket_F = db.ticket_f;
const Formula_F = db.formula_f;
const Status = db.status;
const hash = require("object-hash");

const startTrialSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  details: Joi.array().required(),
  formula: Joi.array().required(),
  product_properties: Joi.array().allow(null),
  result_fg_name: Joi.string().allow(null).allow(""),
  result_fg_code: Joi.string().allow(null).allow(""),
});

const finishTrialSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  details: Joi.array().required(),
  formula: Joi.array().required(),
  product_properties: Joi.array().allow(null),
});

function validateStartTrialSchema(schema) {
  return startTrialSchema.validate(schema);
}

function validateFinishTrialSchema(schema) {
  return finishTrialSchema.validate(schema);
}

async function read(query) {
  try {
    let condition = {};
    condition.is_active = true;
    condition.status_code = {
      $in: [
        "siap ",
        "siap_trial_miniplant",
        "sedang_trial_miniplant",
        "siap_penjadwalan",
      ],
    };
    if (query.target_date_start && query.target_date_finish) {
      condition.schedule_date = {
        $gte: moment(query.target_date_start).startOf("day"),
        $lte: moment(query.target_date_finish).startOf("day"),
      };
    }

    debug(condition, " => condition");

    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "target_date";
    const sort = query.sortBy
      ? {
        [sortField]: sortOrder,
      }
      : {
        target_date: 1,
      };

    //skip
    const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;
    //limit
    const limit = sizePerPage;

    const ck = await Formula_F.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(ck)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await Formula_F.find(condition);
    const result = {
      foundData: ck,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function stratTrial(payload) {
  try {
    const validate = validateStartTrialSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    debug(payload);
    let getFormula = await Formula_F.findOne({
      _id: mongoose.Types.ObjectId(payload._id),
      status_code: { $in: ["siap_trial_miniplant", "siap_penjadwalan"] },
    });

    if (helper.isEmptyObject(getFormula))
      return error.errorReturn({
        message: "Formula is not found, please reload your page",
      });

    let old_formula = getFormula.formula;
    let new_formula = payload.formula;

    let getOldData = await Formula_F.find({ job_number: { $ne: null } })
      .sort({ job_number: -1 })
      .lean();
    let urutan = 0;
    if (!helper.isEmptyArray(getOldData)) {
      urutan = getOldData.length;
    }

    //create job number
    let job_number = "JN" + zeroPad.zeroPad(urutan + 1, 10);

    //get status
    let status = await Status.findOne({
      status_code: "sedang_trial_miniplant",
      is_active: true,
    });
    let status_formula = [
      {
        requester: payload.requester,
        data: null,
        status: status.status_name,
        status_code: status.status_code,
        remark: "Start Trial",
        date: new Date(),
      },
    ];

    //sodiq 10:57 15-01-2023, for product properties
    let _product_properties = [];
    if (payload.product_properties[0]?.properties !== undefined) {
      if (payload.product_properties[0]?.properties.length > 0) {
        for (let obj of payload.product_properties[0]?.properties) {
          if (obj.product_properties.value !== '') {
            _product_properties.push({ properties: obj.product_properties.value, value: obj.value })
          }
        }
      }
    }

    let new_payload = {};
    new_payload.editable = false;
    new_payload.details = payload.details;
    new_payload.formula = payload.formula;
    new_payload.updated_at = new Date();
    new_payload.updated_by = payload.requester;
    new_payload.job_number = job_number;
    new_payload.status = status.status_name;
    new_payload.status_code = status.status_code;
    new_payload.history = getFormula.history
      ? [...getFormula.history, ...status_formula]
      : status_formula;
    if (hash(old_formula) != hash(new_formula)) {
      new_payload.prev_formula = [old_formula];
    }
    new_payload.product_properties = _product_properties
    debug(new_payload);

    //update formula
    let filter = { _id: mongoose.Types.ObjectId(payload._id) };
    let result = await Formula_F.findOneAndUpdate(filter, new_payload, {
      useFindAndModify: false,
      new: true,
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function finishTrial(payload) {
  try {
    debug("finish trial");
    debug(payload);
    const validate = validateFinishTrialSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    let getFormula = await Formula_F.findOne({
      _id: mongoose.Types.ObjectId(payload._id),
      status_code: "sedang_trial_miniplant",
    });
    if (helper.isEmptyObject(getFormula))
      return error.errorReturn({
        message: "Formula is not found, please reload your page",
      });

    //get status
    let status = await Status.findOne({
      status_code: "selesai_trial_miniplant",
      is_active: true,
    });

    let old_formula = getFormula.formula;
    let new_formula = payload.formula;

    let status_formula = [
      {
        requester: payload.requester,
        data: null,
        status: status.status_name,
        status_code: status.status_code,
        remark: null,
        date: new Date(),
      },
    ];

    //sodiq 11:00 15-01-2023, for product properties
    let _product_properties = [];
    if (payload.product_properties[0]?.properties !== undefined) {
      if (payload.product_properties[0]?.properties.length > 0) {
        for (let obj of payload.product_properties[0]?.properties) {
          if (obj.product_properties.value !== '') {
            _product_properties.push({ properties: obj.product_properties.value, value: obj.value })
          }
        }
      }
    }

    let new_payload = {};
    new_payload.details = payload.details;
    new_payload.formula = payload.formula;
    new_payload.updated_at = new Date();
    new_payload.updated_by = payload.requester;
    new_payload.status = status.status_name;
    new_payload.status_code = status.status_code;
    new_payload.history = getFormula.history
      ? [...getFormula.history, ...status_formula]
      : status_formula;
    if (hash(old_formula) != hash(new_formula)) {
      let getOldFOmula = getFormula.prev_formula;
      getOldFOmula.push(old_formula);
      new_payload.prev_formula = getOldFOmula;
    }
    new_payload.product_properties = _product_properties

    //update formula
    let filter = { _id: mongoose.Types.ObjectId(payload._id) };
    let result = await Formula_F.findOneAndUpdate(filter, new_payload, {
      useFindAndModify: false,
      new: true,
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  read,
  stratTrial,
  finishTrial,
};
