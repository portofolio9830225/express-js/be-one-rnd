const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-one-rnd:currency-module");
const mongoose = require("mongoose");
const db = require("../../models");
const Currency = db.currency;

async function readSelect(query) {
  try {
    let condition = {
      is_active: true,
    };

    const ck = await Currency.find(condition)
      .collation({ locale: "en_US", numericOrdering: true })
      .select("-__v");

    let result = ck.map((x) => {
      return {
        value: x.value,
        label: x.value,
      };
    });

    return result;
  } catch (err) {
    debug(err);
    throw error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  readSelect,
};
