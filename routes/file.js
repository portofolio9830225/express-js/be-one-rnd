const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:file-routes");
const fileModule = require("../modules/fileModule");
const BackendValidator = require("../middlewares/BackendValidator");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await fileModule.create(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
