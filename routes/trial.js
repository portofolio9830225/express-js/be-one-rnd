const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:trial-routes");
const trialModule = require("../modules/faiz/trialModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    let result = {};
    result = await trialModule.read(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/start", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await trialModule.stratTrial(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/finish", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await trialModule.finishTrial(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
