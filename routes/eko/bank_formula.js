const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:formula-routes");
const bankFormulaModule = require("../../modules/eko/bankFormulaModule");
const BackendValidator = require("../../middlewares/BackendValidator");
const helper = require("../../common/helper");

const multer = require("multer");
const fs = require("fs");
const path = require("path");
const DIR = "../../public/tmp-upload";

const storage = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, DIR);
  },
  filename(req, file, callback) {
    callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`);
  },
});

const upload = multer({ storage }).array("files");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    let result = {};
    result = await bankFormulaModule.read(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post(
  "/verification",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const x_app_token = req.header("x-application-token");

      // create a directory if it doesn't exist
      if (!fs.existsSync(DIR)) {
        debug("CREATE DIRECTORY");
        fs.mkdirSync(DIR, { recursive: true });
      }

      // upload file to local server
      upload(req, res, async (error) => {
        if (error) {
          debug(error, "===> ERROR");
          return res.status(400).send("Upload Error! Contact Admin!");
        }
        //
        const payload = {
          ...req.body,
          files: req.files,
          app_token: x_app_token,
        };

        const result = await bankFormulaModule.verificationFormula(payload);

        for (let f of payload.files) {
          // remove files when finish make call
          fs.unlinkSync(path.join(__dirname, `/../${f.path}`));
        }

        if (result?.error) {
          return res.status(400).send(result);
        }

        return res.send(result);
      });
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.get("/data", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());

    const result = await bankFormulaModule.readFormula(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/detail", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());
    let result = {};
    result = await bankFormulaModule.readAllFormula(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/select", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    let result = {};
    result = await bankFormulaModule.readAllFormulaWithOutParam(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    let result = {};
    result = await bankFormulaModule.formulasi(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/next/trial", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    let result = {};
    result = await bankFormulaModule.nextTrialProcess(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/sensory", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await bankFormulaModule.sensoryFormula(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put(
  "/sensory/plant",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.body;

      const result = await bankFormulaModule.sensoryPlantFormula(payload);

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.put(
  "/update-status",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.body;

      const result = await bankFormulaModule.updateStatusFormula(payload);

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    let result = {};
    result = await bankFormulaModule.deleteFormula(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
