const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:schedule-routes");
const scheduleModule = require("../modules/faiz/scheduleModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    let result = {};
    result = await scheduleModule.read(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await scheduleModule.schedule(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
