const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-checklist:payment-remark-routes");
const sampleIncomingRemarkModule = require("../../modules/rizky/sampleIncomingRemarkModule");
const BackendValidator = require("../../middlewares/BackendValidator");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await sampleIncomingRemarkModule.readAll(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
