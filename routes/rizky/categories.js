const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-checklist:payment-remark-routes");
const categoriesModule = require("../../modules/rizky/categoriesModule");
const helper = require("../../common/helper");

router.get("/", async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await categoriesModule.readAllCategories(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/", async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await categoriesModule.createCategory(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
