const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-checklist:email-routes");
const generalSettingsModule = require("../../modules/rizky/generalSettingsModule");
const BackendValidator = require("../../middlewares/BackendValidator");

router.post("/", async (req, res) => {
  try {
    const payload = req.query;
    let result = {};
    result = await generalSettingsModule.emailNotification(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
