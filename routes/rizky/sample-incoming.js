const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:registrasi-ticket-module");
const sampleIncomingtModule = require("../../modules/rizky/sampleIncomingModule");
const helper = require("../../common/helper");
const BackendValidator = require("../../middlewares/BackendValidator");
const multer = require("multer");
const DIR = "./public/tmp-upload";
const stream = require("stream");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    cb(null, `${file.fieldname}${Date.now()}${file.originalname}`);
  },
});

const upload = multer({ storage }).array("files");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    
    payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());
    let result = {};
    result = await sampleIncomingtModule.readAllSampleIncoming(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/download", async (req, res) => {
  try {
    const x_app_token_ = req.header("x-application-token");
    const payload = {
      id: req.query,
      app_token: x_app_token_,
    };
    let result = {};
    result = await sampleIncomingtModule.downloadFile(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  const currentUser = helper.getCurrrentUser(req);
  try {
    const x_app_token_ = req.header("x-application-token");

    upload(req, res, async (err) => {
      if (err) {
        debug(err);
        return res.status(400).send("Upload Error! Contact Admin!");
      }

      const payload = {
        ...req.body,
        files: req.files,
        app_token: x_app_token_,
      };

      debug(payload, " this is payload");

      const result = await sampleIncomingtModule.createSampleIncoming(
        payload,
        currentUser
      );

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    });
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/copy", BackendValidator.isValidRequest, async (req, res) => {
  const currentUser = helper.getCurrrentUser(req);
  try {
    const x_app_token_ = req.header("x-application-token");

    upload(req, res, async (err) => {
      if (err) {
        debug(err);
        return res.status(400).send("Upload Error! Contact Admin!");
      }

      const payload = {
        ...req.body,
        files: req.files,
        app_token: x_app_token_,
      };

      debug(payload, " this is payload");

      const result = await sampleIncomingtModule.copySampleIncoming(
        payload,
        currentUser
      );

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    });
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  const currentUser = helper.getCurrrentUser(req);
  try {
    const x_app_token_ = req.header("x-application-token");

    upload(req, res, async (err) => {
      if (err) {
        debug(err);
        return res.status(400).send("Upload Error! Contact Admin!");
      }

      const payload = {
        ...req.body,
        files: req.files,
        app_token: x_app_token_,
      };

      debug(payload, " this is payload");

      const result = await sampleIncomingtModule.updateSampleIncoming(
        payload,
        currentUser
      );

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    });
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", async (req, res) => {
  try {
    const payload = req.body;
    const result = await sampleIncomingtModule.deleteSampleIncoming(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
