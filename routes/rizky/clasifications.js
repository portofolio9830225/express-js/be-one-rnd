const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-checklist:payment-remark-routes");
const clasifications = require("../../modules/rizky/clasificationsModule");
const BackendValidator = require("../../middlewares/BackendValidator");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await clasifications.list(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
