const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-checklist:payment-remark-routes");
const additionalInformationModule = require("../../modules/rizky/additionalInformationModule");
const BackendValidator = require("../../middlewares/BackendValidator");
const helper = require("../../common/helper");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await additionalInformationModule.readAllAdditionalInformation(
      payload
    );
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
