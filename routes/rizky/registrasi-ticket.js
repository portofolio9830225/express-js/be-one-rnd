const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:registrasi-ticket-module");
const registrasiTicketModule = require("../../modules/rizky/registrasiTicketModule");
const helper = require("../../common/helper");
const BackendValidator = require("../../middlewares/BackendValidator");
const multer = require("multer");
const DIR = "./public/tmp-upload";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    cb(null, `${file.fieldname}${Date.now()}${file.originalname}`);
  },
});

const upload = multer({ storage }).array("files");

router.post("/upload", BackendValidator.isValidRequest, async (req, res) => {
  const currentUser = helper.getCurrrentUser(req);
  console.log(req.files, ">>>> files");
  try {
    const x_app_token_ = req.header("x-application-token");

    upload(req, res, async (err) => {
      if (err) {
        debug(">>>> this is err");
        debug(err);
        return res.status(400).send("Upload Error! Contact Admin!");
      }

      let id = "";
      Object.entries(req.query).forEach(([key, val]) => {
        id = key;
      });

      const payload = {
        files: req.files,
        id: id,
        app_token: x_app_token_,
      };

      const result = await registrasiTicketModule.upload(payload, currentUser);

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    });
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post(
  "/uploadSelesai",
  BackendValidator.isValidRequest,
  async (req, res) => {
    const currentUser = helper.getCurrrentUser(req);
    console.log(req.files, ">>>> files");
    try {
      const x_app_token_ = req.header("x-application-token");

      upload(req, res, async (err) => {
        if (err) {
          debug(">>>> this is err");
          debug(err);
          return res.status(400).send("Upload Error! Contact Admin!");
        }

        let id = "";
        Object.entries(req.query).forEach(([key, val]) => {
          id = key;
        });

        const payload = {
          files: req.files,
          id: id,
          app_token: x_app_token_,
        };

        const result = await registrasiTicketModule.uploadSelesaiAnalisa(
          payload,
          currentUser
        );

        if (!result.error) {
          return res.send(result);
        } else {
          return res.status(400).send(result);
        }
      });
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await registrasiTicketModule.readAllTicket(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/get-ticket", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    console.log("log hehe");
    let result = {};
    result = await registrasiTicketModule.readByIdTicket(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  const currentUser = helper.getCurrrentUser(req);
  try {
    const payload = req.body;

    const result = await registrasiTicketModule.createTicket(
      payload,
      currentUser
    );
    debug(result);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await registrasiTicketModule.updateRegistrasiTicket(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.patch("/", BackendValidator.isValidRequest, async (req, res) => {
  const currentUser = helper.getCurrrentUser(req);
  try {
    const payload = req.body;
    const result = await registrasiTicketModule.uploadFormAnalisa(
      payload,
      currentUser
    );
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await registrasiTicketModule.deleteTicket(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post(
  "/proses-formulasi",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.body;
      const result = await registrasiTicketModule.prosesForumulasi(payload);

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);
module.exports = router;
