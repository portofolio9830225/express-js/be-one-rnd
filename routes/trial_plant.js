const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:trial-plant-routes");
const trialPlantModule = require("../modules/faiz/trialPlantModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

const multer = require("multer");
const fs = require("fs");
const path = require("path");
const DIR = "./public/tmp-upload";

const storage = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, DIR);
  },
  filename(req, file, callback) {
    callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`);
  },
});

const upload = multer({ storage }).array("files");

router.put("/start", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const x_app_token = req.header("x-application-token");
    // upload file to local server

    upload(req, res, async (error) => {
      if (error) {
        debug(error, "===> ERROR");
        return res.status(400).send("Upload Error! Contact Admin!");
      }
      //
      const payload = {
        ...req.body,
        files: req.files,
        app_token: x_app_token,
      };
      debug(payload, " payload");

      const result = await trialPlantModule.stratTrial(payload);

      for (let f of payload.files) {
        // remove files when finish make call
        fs.unlinkSync(path.join(__dirname, `/../${f.path}`));
      }

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    });
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/finish", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const x_app_token = req.header("x-application-token");
    // upload file to local server

    upload(req, res, async (error) => {
      if (error) {
        debug(error, "===> ERROR");
        return res.status(400).send("Upload Error! Contact Admin!");
      }
      const payload = {
        ...req.body,
        files: req.files,
        app_token: x_app_token,
      };
      debug(payload, " payload");

      const result = await trialPlantModule.finishTrial(payload);

      for (let f of payload.files) {
        // remove files when finish make call
        fs.unlinkSync(path.join(__dirname, `/../${f.path}`));
      }

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    });
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
