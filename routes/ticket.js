const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:ticket-routes");
const ticketModule = require("../modules/ticketModule");
const BackendValidator = require("../middlewares/BackendValidator");

router.get("/id/:id", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const x_app_token = req.header("x-application-token");
    const payload = req.params;
    if (req.query) {
      payload.ticket_id = req.query.ticket_id;
    }

    const result = await ticketModule.readById(payload, x_app_token);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
