const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:customers-module");
const formulasModule = require("../../modules/azhar/formulasModule");

const BackendValidator = require("../../middlewares/BackendValidator");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;

		const result = await formulasModule.readAll(payload);
		return res.status(200).send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

module.exports = router;
