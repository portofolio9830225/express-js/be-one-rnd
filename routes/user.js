const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:user-routes");
const userModule = require("../modules/userModule");
const BackendValidator = require("../middlewares/BackendValidator");

// get all user detail by dept
router.get("/detail", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    payload.access_token = req.headers["x-user-token"].split(" ")[1];
    let result = {};
    result = await userModule.getUserByDept(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get(
  "/detail/filter",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.query;
      payload.access_token = req.headers["x-user-token"].split(" ")[1];
      let result = {};
      result = await userModule.getUserByDeptwithFilter(payload);
      debug(result, " res result route");
      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

// get send notif user
router.get("/notif", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const result = await userModule.getSendNotifUser();
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// get all department
router.get("/department", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    payload.access_token = req.headers["x-user-token"].split(" ")[1];
    let result = {};
    result = await userModule.getAllDepartment(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// get downline by
router.get("/downline", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    const result = await userModule.getDownline(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// get downline select
router.get(
  "/downline/select",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.query;
      const result = await userModule.getDownlineSelect(payload);

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

// get downline select mobile
router.get(
  "/downline/select/mobile",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.query;
      const result = await userModule.getDownlineSelectMobile(payload);

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

// get role by parent
router.get(
  "/role_by_parent",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.query;
      const result = await userModule.getUserRoleByParent(payload);

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

// create data user
router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await userModule.create(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// read data user
router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    payload.show_all = ["true", "1"].includes(payload?.show_all.toLowerCase());

    const result = await userModule.read(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// read data select user
router.get("/select", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;

    const result = await userModule.readSelect(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// update data user
router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await userModule.update(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// delete data user
router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await userModule.destroy(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

// find user
router.get("/find", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;

    const result = await userModule.findUser(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get(
  "/select/parent",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.query;
      const result = await userModule.getUpperParent(payload);

      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.put("/last-login", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await userModule.updateLastLogin(payload);
    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/token", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await userModule.updateFcmToken(payload);
    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/single", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    const result = await userModule.getSingleUser(payload);
    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/profile", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await userModule.updateProfile(payload);
    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/password", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const x_app_token = req.header("x-application-token");
    const x_user_token = req.header("x-user-token");
    payload.app_token = x_app_token;
    payload.user_token = x_user_token;
    const result = await userModule.updatePassword(payload);
    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
