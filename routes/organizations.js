const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-one-rnd:menu-routes");
const organizationModule = require("../modules/organizationModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await organizationModule.read(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);

    const result = await organizationModule.create(payload);
    debug(result);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await organizationModule.update(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await organizationModule.destroy(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
