require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const fs = require("fs");
const helmet = require("helmet");
const debug = require("debug")("backend-one-rnd:server");
const error = require("./common/errorMessage");
const rfs = require("rotating-file-stream");
const app = express();
const cors = require("cors");
const cron = require("node-cron");

app.use(cors());

if (process.env.NODE_ENV === "production") {
	const accessLogStream = rfs.createStream("server.log", {
		interval: "1d", // rotate daily
		path: path.join(__dirname, "log"),
	});
	app.use(logger("combined", { stream: accessLogStream }));
} else {
	app.use(logger("combined"));
}
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(bodyParser.json({ limit: "5mb" }));

app.use(express.static(path.join(__dirname, "public")));

const db = require("./models");
let MONGO_URI = process.env.NODE_ENV == "test" ? process.env.MONGO_CLOUD_TEST : process.env.MONGO_CLOUD;

debug(`will connect to : ${MONGO_URI}`);
db.mongoose
	.connect(`${MONGO_URI}`, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		auth: { authSource: "admin" },
		user: `${process.env.MONGO_USERNAME}`,
		pass: `${process.env.MONGO_PASSWORD}`,
	})

	.then(() => {
		debug("Successfully connect to MongoDB.");
	})
	.catch(err => {
		debug("Connection error", err);
		process.exit();
	});
const arrayAllowEnv = ["production", "staging", "development"];
if (arrayAllowEnv.includes(process.env.NODE_ENV)) {
	//cron
}
//checking health pod
app.get("/health", (req, res) => {
	res.send(`Apps is healthy`);
});

// rizky routes
const registrasiTicketRouter = require("./routes/rizky/registrasi-ticket");
app.use("/api/ticket", registrasiTicketRouter);
const customerRouter = require("./routes/rizky/customers");
app.use("/api/customers", customerRouter);
const customerTicketRouter = require("./routes/rizky/customers_ticket");
app.use("/api/customers-ticket", customerTicketRouter);
const categoriesRouter = require("./routes/rizky/categories");
app.use("/api/categories", categoriesRouter);
const currenciesRouter = require("./routes/rizky/currencies");
app.use("/api/currencies", currenciesRouter);
const additionalInformationRouter = require("./routes/rizky/additional-information");
app.use("/api/additional-information", additionalInformationRouter);
const sampleIncomingRouter = require("./routes/rizky/sample-incoming");
app.use("/api/sample-incoming", sampleIncomingRouter);
const clasificationRouter = require("./routes/rizky/clasifications");
app.use("/api/clasifications", clasificationRouter);
const ticketRemarkRouter = require("./routes/rizky/ticket_remark");
app.use("/api/ticket-remark", ticketRemarkRouter);

app.get("/", (req, res) => {
	res.send(`Welcome to Backend HRIS`);
});

const formulasRouter = require("./routes/azhar/formula");
app.use("/api/azhar/formula", formulasRouter);

const authRouter = require("./routes/auth");
app.use("/api/auth", authRouter);
const menuRouter = require("./routes/menu");
app.use("/api/menu", menuRouter);
const submenuRouter = require("./routes/submenu");
app.use("/api/submenu", submenuRouter);
const authorizationRouter = require("./routes/authorization");
app.use("/api/authorization", authorizationRouter);

const userRouter = require("./routes/user");
app.use("/api/user", userRouter);
const fileRouter = require("./routes/file");
app.use("/api/file", fileRouter);

const organizationRouter = require("./routes/organizations");
app.use("/api/user", organizationRouter);

const scheduleRouter = require("./routes/schedule");
app.use("/api/schedule", scheduleRouter);
const trialRouter = require("./routes/trial");
app.use("/api/trial", trialRouter);
const trialPlantRouter = require("./routes/trial_plant");
app.use("/api/trial_plant", trialPlantRouter);
const formulaRouter = require("./routes/formula");
app.use("/api/formula", formulaRouter);
const priorityRouter = require("./routes/priority");
app.use("/api/priority", priorityRouter);
const materialRouter = require("./routes/material");
app.use("/api/material", materialRouter);
const vendorRouter = require("./routes/vendor");
app.use("/api/vendor", vendorRouter);
const currencyRouter = require("./routes/faiz/currency");
app.use("/api/currency", currencyRouter);
const addInfromationRouter = require("./routes/faiz/additional_information");
app.use("/api/add_information", addInfromationRouter);
const poDetailRouter = require("./routes/po_detail");
app.use("/api/po_detail", poDetailRouter);
const emailRouter = require("./routes/rizky/emailRoute");
app.use("/api/email", emailRouter);
const sampleIncomingRemarkRouter = require("./routes/rizky/sample_incoming_remark");
app.use("/api/sample-incoming-remark", sampleIncomingRemarkRouter);
const sampleIncomingDocumentRouter = require("./routes/sample_incoming_document");
app.use("/api/sample_incoming_document", sampleIncomingDocumentRouter);

const ticketRouter = require("./routes/ticket");
app.use("/api/tickets", ticketRouter);

const uomRouter = require("./routes/faiz/uom");
app.use("/api/uom", uomRouter);

// eko
const bankFormulaRouter = require("./routes/eko/bank_formula");
app.use("/api/bank_formula", bankFormulaRouter);

app.use((req, res, next) => {
	return res.status(404).send(error.errorReturn({ message: "Invalid routes!" }));
});
module.exports = app;
