const moment = require("moment");
function getNextMonthList(start_month, numMonth) {
  const firstMonth = moment(start_month);
  let nextSixMonth = firstMonth.add(numMonth, "month");
  let dates = [];
  const cloneFirstMonth = moment(start_month);
  while (cloneFirstMonth < nextSixMonth) {
    dates.push(cloneFirstMonth.format("YYYY-MM-DD"));
    cloneFirstMonth.add(1, "month");
  }
  return dates;
}

async function checkDuration(payload) {
  return parseInt(
    (Math.abs(
      new Date(payload.sdate_finish).getTime() -
        new Date(payload.sdate_start).getTime()
    ) /
      (1000 * 60)) %
      60
  );
}

async function checkTimeDuration(date_end, date_start) {
  return parseInt(
    Math.abs(new Date(date_end).getTime() - new Date(date_start).getTime()) /
      (1000 * 60)
  );
}

async function checkDurationDay(date1, date2) {
  return parseInt(
    Math.round((new Date(date1) - new Date(date2)) / (24 * 60 * 60 * 1000))
  );
}

async function checkTimeBefore(time1, time2) {
  return moment(time1, "HH:mm").isBefore(moment(time2, "HH:mm"));
}

async function addtime(date, duration) {
  return new Date(date).getTime() + duration * 60000;
}

async function addDays(date, days) {
  return new Date(date).setDate(new Date(date).getDate() + days);
}

function addSeconds(date, seconds) {
  return new Date(date).setSeconds(new Date(date).getSeconds() + seconds);
}

async function checkTimeBetween(time1, time2, time3) {
  return moment(time1, "HH:mm").isBetween(
    moment(time2, "HH:mm"),
    moment(time3, "HH:mm")
  );
}

function isSameDay(date1, date2) {
  return (
    new Date(date1).getFullYear() === new Date(date2).getFullYear() &&
    new Date(date1).getMonth() === new Date(date2).getMonth() &&
    new Date(date1).getDate() === new Date(date2).getDate()
  );
}

function diff_hours(date1, date2) {
  var diff = (new Date(date1).getTime() - new Date(date2).getTime()) / 1000;
  diff /= 60 * 60;
  return Math.abs(diff);
}

module.exports = {
  getNextMonthList,
  checkDuration,
  checkTimeDuration,
  addtime,
  checkTimeBefore,
  addDays,
  checkDurationDay,
  addSeconds,
  checkTimeBetween,
  isSameDay,
  diff_hours,
};
