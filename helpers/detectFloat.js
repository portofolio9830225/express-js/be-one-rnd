const accounting = require("./accounting");

async function detectFloat(source) {
  let float = accounting.unformat(source, ",");
  if (source) {
    let posComma = source.toString().indexOf(",");
    if (posComma > -1) {
      let posDot = source.toString().indexOf(".");
      if (posDot > -1 && posComma > posDot) {
        let germanFloat = accounting.unformat(source, ",");
        if (Math.abs(germanFloat) > Math.abs(float)) {
          float = parseFloat(germanFloat);
        }
      } else {
        float = parseFloat(accounting.unformat(source, ","));
      }
    } else {
      float = !isNaN(source) ? parseFloat(source) : source.replace(/,/g, ".");
    }
  }
  return float;
}
module.exports = {
  detectFloat,
};
