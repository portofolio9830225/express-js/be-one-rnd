require("dotenv").config();
const schemaCategory = {
  type: "object",
  properties: {
    _id: { type: "string" },
    is_active: { type: "boolean" },
    name: { type: "string" },
  },
};

module.exports = {
  schemaCategory,
};
