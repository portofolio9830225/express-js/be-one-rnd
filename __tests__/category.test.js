const env = require("dotenv").config();
process.env.NODE_ENV = "test";

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");
const {
  schemaCategory,
} = require("./schemaValidator/category_schemaValidator");

const debug = require("debug")("backend-one-rnd:ticket-test"); //jest json schema
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE CATEGORY", () => {
  it("POST /api/categories", async () => {
    // create Category
    const createCategory = await reqWithAuth("/api/categories", "post", {
      name: "New Product",
      is_active: true,
    });
    expect(createCategory.status).toEqual(200);
    expect(createCategory.body).toMatchSchema(schemaCategory);
  });
});
