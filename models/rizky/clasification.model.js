const { toArray } = require("lodash");
const mongoose = require("mongoose");
const ClasificationModel = mongoose.model(
    "Clasifications",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "clasifications"
);
module.exports = ClasificationModel;