const { toArray } = require("lodash");
const mongoose = require("mongoose");
const CustomerTicketModel = mongoose.model(
    "Customers Ticket",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "customer_ticket"
);
module.exports = CustomerTicketModel;
