const { toArray } = require("lodash");
const mongoose = require("mongoose");
const TicketRemarkModel = mongoose.model(
    "Ticket Remark",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "ticket_remark"
);
module.exports = TicketRemarkModel;
