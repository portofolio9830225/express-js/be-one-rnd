const mongoose = require("mongoose");
const SampleIncoming = mongoose.model(
  "Sample Incoming",
  new mongoose.Schema({
    material_name: { type: String, required: true },
    clasification: { type: String, required: true },
    manufacture: { type: String },
    supplier: { type: String },
    origin: { type: String },
    packaging: { type: String },
    date_receive: { type: Date },
    documnet: { type: String },
    list_document: { type: String },
    remark: { type: String },
    Note: { type: String },
    pic: { type: String },
    pic_name: { type: String },
    is_active: { type: Boolean, required: true },
    qty: { type: String, required: true },
    uom: { type: String, required: true },
    location: { type: String, required: true },
    created_at: { type: Date, required: true },
    created_by: { type: String, required: true },
    updated_at: { type: Date, required: true },
    send_notif: Array,
  }),
  "sample_incoming"
);
module.exports = SampleIncoming;
