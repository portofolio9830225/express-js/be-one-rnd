const { toArray } = require("lodash");
const mongoose = require("mongoose");
const CustomerModel = mongoose.model(
    "Customer",
    new mongoose.Schema({
        code: { type: String },
        _last_updated_at: Date,
        _loaded_at: Date,
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "customers"
);
module.exports = CustomerModel;
