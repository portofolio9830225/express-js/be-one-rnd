const { toArray } = require("lodash");
const mongoose = require("mongoose");
const CurrenciesModel = mongoose.model(
    "Currencies",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "currencies"
);
module.exports = CurrenciesModel;