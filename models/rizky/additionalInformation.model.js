const { toArray } = require("lodash");
const mongoose = require("mongoose");
const AdditionalInformationModel = mongoose.model(
    "Additional Information",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "additional_information"
);
module.exports = AdditionalInformationModel;