const { toArray } = require("lodash");
const mongoose = require("mongoose");
const CategoriesModel = mongoose.model(
    "Categoires",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "categories"
);
module.exports = CategoriesModel;