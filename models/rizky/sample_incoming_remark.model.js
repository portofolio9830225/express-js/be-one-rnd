const { toArray } = require("lodash");
const mongoose = require("mongoose");
const SampleIncomingRemarkModel = mongoose.model(
    "Sample Incoming Remark",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "sample_incoming_remark"
);
module.exports = SampleIncomingRemarkModel;