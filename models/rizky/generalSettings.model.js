const mongoose = require("mongoose");
const GeneralSettings = mongoose.model(
  "General Settings",
  new mongoose.Schema({
    key: { type: String },
    value: { type: String },
    remark: { type: String },
    is_active: { type: Boolean, required: true },
  }),
  "general_settings"
);
module.exports = GeneralSettings;
