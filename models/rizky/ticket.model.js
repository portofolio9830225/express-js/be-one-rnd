const { toArray } = require("lodash");
const mongoose = require("mongoose");
const TicketModel = mongoose.model(
    "Ticket",
    new mongoose.Schema({
        is_active: { type: Boolean },
        is_used: { type: Boolean },
        is_deleted: { type: Boolean },
        document_number: { type: String },
        target_price: { type: Number },
        currency: { type: String },
        date_of_communication: Date,
        date: Date,
        to: { type: String },
        sample_product: { type: Boolean },
        customer_code: { type: String },
        customer_name: { type: String },
        category: { type: String },
        target_date: Date,
        status: { type: String },
        status_code: { type: String },
        information: { type: Array },
        sample: { type: Array },
        pic: { type: String },
        pic_name: { type: String },
        note: { type: String },
        initial_project: { type: String },
        history: { type: Array },
        created_by: { type: String },
        updated_by: { type: String },
        created_at: Date,
        updated_at: Date
    }),
    "tickets"
);
module.exports = TicketModel;
