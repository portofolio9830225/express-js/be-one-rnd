const mongoose = require("mongoose");
const Roles = mongoose.model(
  "Roles",
  new mongoose.Schema({
    is_active: { type: Boolean, required: true },
    role_code: { type: String, required: true },
    role_name: { type: String, required: true },
    segment_code: { type: String, required: true },
    segment_name: { type: String, required: true },
    store_segment: { type: Object, required: true },
    parent_role_code: { type: String },
    parent_role_name: { type: String },
    created_at: Date,
    created_by: { type: String },
    updated_at: Date,
    updated_by: { type: String },
    deleted_at: Date,
    deleted_by: { type: String },
  }),
  "roles"
);
module.exports = Roles;
