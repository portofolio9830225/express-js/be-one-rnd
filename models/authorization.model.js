const mongoose = require("mongoose");
const Authorization = mongoose.model(
  "Authorization",
  new mongoose.Schema({
    role: { type: String, required: true },
    menu: { type: Object, required: true },
    menu_mobile: { type: Array },
    isDeleted: { type: Boolean },
  }),
  "authorizations"
);
module.exports = Authorization;
