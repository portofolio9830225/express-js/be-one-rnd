const mongoose = require("mongoose");
const SampleIncomingDocument = mongoose.model(
  "SampleIncomingDocument",
  new mongoose.Schema({
    name: { type: String, required: true },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
  }),
  "sample_incoming_document"
);
module.exports = SampleIncomingDocument;
