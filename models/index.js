const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;

db.set_active_log = require("./set_active_log.model");
db.authorization = require("./authorization.model");
db.menu = require("./menu.model");
db.submenu = require("./submenu.model");
db.user = require("./user.model");
db.authentication = require("./authentication.model");
db.authentication_history = require("./authentication_history.model");
db.file = require("./file.model");
db.role = require("./roles.model");
db.ticket_f = require("./faiz/ticket.model");
db.priority = require("./priority.model");
db.status = require("./status.model");
db.formula_f = require("./faiz/formula.model");
db.additional_information_f = require("./faiz/additional_information.model");
db.material = require("./material.model");
db.vendor = require("./vendor.model");
db.currency = require("./faiz/currency.model");
db.uom = require("./faiz/uom.model");
db.po_detail = require("./po_detail.model");
db.sample_incoming_document = require("./sample_incoming_document.model");

// rizky
db.ticket = require("./rizky/ticket.model");
db.customer = require("./rizky/customers.model");
db.customers_ticket = require("./rizky/customers_ticket.model");
db.categories = require("./rizky/categories.model");
db.currencies = require("./rizky/currency.model");
db.additional_information = require("./rizky/additionalInformation.model");
db.sample_incoming = require("./rizky/sample_incoming.model");
db.clasification = require("./rizky/clasification.model");
db.general_settings = require("./rizky/generalSettings.model");
db.sample_incoming_remark = require("./rizky/sample_incoming_remark.model");
db.ticket_remark = require("./rizky/ticket_remark.model");

db.organizations = require("./organization.model");

// eko
db.formula_e = require("./eko/formula.model");

module.exports = db;
