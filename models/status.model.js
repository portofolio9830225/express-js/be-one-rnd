const mongoose = require("mongoose");
const Status = mongoose.model(
  "Status",
  new mongoose.Schema({
    status_code: { type: String, required: true },
    status_name: { type: String, required: true },
    ordering: { type: String, required: true },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
  }),
  "status"
);
module.exports = Status;
