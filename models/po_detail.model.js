const mongoose = require("mongoose");
const Po_details = mongoose.model(
  "Po_details",
  new mongoose.Schema({
    po_number: { type: String, required: true },
    po_date: { type: String, required: true },
    po_vendor: { type: String },
    item_no: { type: String },
    material_code: { type: String },
    material_desc: { type: String },
    qty: { type: Number },
    uom: { type: String },
    net_price: { type: Number },
    total_price: { type: Number },
    currency: { type: String },
    remaining_invoice: { type: String },
    remaining_paid: { type: Number },
    is_active: { type: Boolean, required: true },
  }),
  "po_details"
);
module.exports = Po_details;
