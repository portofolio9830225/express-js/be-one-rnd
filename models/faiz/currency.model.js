const { toArray } = require("lodash");
const mongoose = require("mongoose");
const Currency = mongoose.model(
  "Currency",
  new mongoose.Schema({
    is_active: { type: Boolean },
    value: { type: String },
  }),
  "currencies"
);
module.exports = Currency;
