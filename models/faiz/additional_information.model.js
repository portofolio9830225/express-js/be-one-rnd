const mongoose = require("mongoose");
const AdditionalInformation = mongoose.model(
  "AdditionalInformation",
  new mongoose.Schema({
    name: { type: String, required: true },
    type: { type: String, required: true },
    category: { type: String, required: true },
    is_active: { type: Boolean },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
  }),
  "additional_information"
);
module.exports = AdditionalInformation;
