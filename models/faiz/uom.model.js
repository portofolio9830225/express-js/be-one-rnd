const mongoose = require("mongoose");
const Uom = mongoose.model(
  "Uom",
  new mongoose.Schema({
    name: { type: String, required: true },
    code: { type: String, required: true },
    is_active: { type: Boolean, required: true },
  }),
  "uom"
);
module.exports = Uom;
