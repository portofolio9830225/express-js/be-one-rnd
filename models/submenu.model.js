const mongoose = require("mongoose");  
const Submenu = mongoose.model(
  "Submenu",
  new mongoose.Schema({
    name: {type:String,required:true},
    link: {type:String,required:true},
    isDeleted: {type: Boolean},
    created_at: Date,
    updated_at: Date, 
    menu: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Menu"
    }
  }),
  "submenus"
); 
module.exports = Submenu;
