const mongoose = require("mongoose");
const Vendor = mongoose.model(
  "Vendor",
  new mongoose.Schema({
    code: { type: String, required: true },
    name: { type: String, required: true },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
  }),
  "vendors"
);
module.exports = Vendor;
