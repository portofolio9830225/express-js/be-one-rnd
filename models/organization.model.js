const mongoose = require("mongoose");

const Organizations = mongoose.model(
  "Organizations",
  new mongoose.Schema({
    organization_code: { type: String, required: true },
    organization_name: { type: String, required: true },
    is_active: { type: Boolean, required: true },
    created_at: Date,
    created_by: { type: String },
    updated_at: Date,
    updated_by: { type: String },
    deleted_at: Date,
    deleted_by: { type: String },
  }),
  "organizations"
);
module.exports = Organizations;
