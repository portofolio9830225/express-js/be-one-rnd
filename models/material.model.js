const mongoose = require("mongoose");
const Material = mongoose.model(
  "Material",
  new mongoose.Schema({
    material_number: { type: String, required: true },
    material_desc: { type: String, required: true },
    material_type: { type: String, required: true },
    uom: { type: String, required: true },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
  }),
  "materials"
);
module.exports = Material;
