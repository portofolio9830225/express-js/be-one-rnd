const mongoose = require("mongoose");  
const Menu = mongoose.model(
  "Menu",
  new mongoose.Schema({
    name: {type:String,required:true},
    icon: {type:String,required:true},
    link: {type:String,required:true},
    isDeleted: {type: Boolean},
    created_at: Date,
    updated_at: Date
  }),
  "menus"
); 
module.exports = Menu;
