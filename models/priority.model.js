const mongoose = require("mongoose");
const Priority = mongoose.model(
  "Priority",
  new mongoose.Schema({
    name: { type: String, required: true },
    color: { type: String, required: true },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
  }),
  "priorities"
);
module.exports = Priority;
